Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	COB Assembly	#	0	#	0	Test Item	#	All	Initialize
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP28				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Search COB Assembly	#	#	#	#	COB	#	#	Search Product Type
Public	Check Station	#	#	#	#	COB	#	#	Check Station	12
Public	Set Station Done	#	#	#	#	COB	#	#	Set Station Done	12
Public	Get Current	#	0	0	#	Hardware	#	0	Get Current	3												
None	Close	#	#	#	0	#	#	#	-------Close Step-------							
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C					
#Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF			
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect									
End	#	#	#	#	0	#	#	#	Test Done						
