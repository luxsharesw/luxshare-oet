Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.1.0
Public	AOC TRx TEST	#	0	#	0	Test Item	#	1	初始化表格	RSSI						
None	Init	#	#	#	0	#	#	#	-------初始程序-------						
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	100G TRx CH1				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH2	Power Supply	100G TRx CH2				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH3	Power Supply	100G TRx CH3				
Public	Set Power State	#	#	#	1000	Hardware	#	#	开启电源供应器	ON					
Public	Search OutSourcing Product	#	#	#	100	AOC	#	#	搜寻产品	SFP28		#Auto Reset Table
Public	Write Outsourcing Password	#	#	#	1000	AOC	#	#	写入密码	SFP10	123				
Public	OutSourcing Get Lot Number	#	#	#	0	AOC	#	#	Get Lot Number	PNSN					
Public	Connect	3	#	#	100	Hardware	#	#	连接误码仪	BERT					
Public	Connect	3	#	#	0	Hardware	#	#	连接电示坡器	Scope
Public	Target Temp	3	85	65	100	AOC	#	All	确认产品温度	360			10	3					
None	Start	#	#	#	0	#	#	#	-------测试程序-------						
Public	Get Voltage	#	#	#	100	AOC	#	#	读取产品电压						
Public	Configure	3	#	#	100	Hardware	#	#	设置误码仪	BERT	SFP10				
Public	Get Error Data	0	#	#	0	Hardware	#	#	误码仪测试	Sync					
None	#	#	#	#	0	#	#	#	-------电示坡器程序-------						
Public	Configure	#	#	#	0	Hardware	#	#	设置电示坡器	Scope	AOC				
Public	Waveforms	#	#	#	0	Hardware	#	All	电示坡器测试	800	Show				
Public	Get Measurement	#	#	#	0	Hardware	#	4	取得电示坡器数据	AOC					
Public	Verify Measurement	#	#	#	0	Hardware	#	#	验证电示坡器数据	AOC					
Public	Save Screen	#	#	#	0	Hardware	#	#	存取电眼图	SFP28 AOC ORT TEST	AOC				
Public	Wait Sync	#	#	#	0	Function	#	#	等待仪器同步						
Public	OutSourcing Check RSSI	#	50000	2500	100	AOC	10		Get RSSI	RSSI					
None	Close	#	#	#	0	#	#	#	-------关闭程序-------						
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	SFP28 AOC ORT TEST	Sync				
Public	Column Status	#	#	#	0	Function	#	#	设置表格	AOC					
Public	Error Code	#	#	#	0	AOC	#	All	取得错误码						
Public	Close USB-I2C	#	#	#	100	AOC	#	#	关闭USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪器						
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败	请注意！ ！测试板寿命即将用尽，请准备！ ！目前剩余次数为	#DEBUG1		
