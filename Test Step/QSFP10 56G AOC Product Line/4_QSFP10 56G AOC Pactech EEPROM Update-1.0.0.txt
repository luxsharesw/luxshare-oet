Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC EEPROM Update	#	0	#	0	Test Item	#	All	初始化表格						
None	Init	#	#	#	0	#	#	#	-------初始程序-------						
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	AOC EEPROM CH1				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH2	Power Supply	AOC EEPROM CH2				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH3	Power Supply	AOC EEPROM CH3				
Public	Set Power State	#	#	#	1000	Hardware	#	#	开启电源供应器	ON					
None	Start	#	#	#	0	#	#	#	-------测试程序-------						
Public	Search Product	#	#	#	0	AOC	#	#	搜寻产品	OQC		#Auto Reset Table	QSFP10
Public	Input Parameter	#	#	#	0	MES	#	#	确认MES系统参数	False					
Public	Move In	#	#	#	0	MES	#	#	访问MES系统	T00068	#DEBUG	访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s	无法访问MES系统,请确认网路或MES连线状态	MES系统没有回应,请确认网路或MES连线状态
Public	Get Service Info	#	#	#	0	MES	#	#	确认测板寿命	测板寿命已用尽，请更换	#DEBUG	
Public	Set Station	#	#	#	0	AOC	#	#	写入产品测试状态	2					
Public	Set Customer Password	#	#	#	0	AOC	#	#	写入客制密码	Pactech					
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	取得产品温度范围						
Public	Verify PN Temp Define	#	#	#	0	AOC	#	#	确认产品温度范围						
Public	EEPROM Update	#	#	#	100	AOC	#	#	更新EEPROM	Custom	QSFP10-56G_AOC-Pactech	C-Temp			
Public	Set Power State	#	#	#	500	Hardware	#	#	关闭电源供应器	OFF					
Public	Set Power State	#	#	#	500	Hardware	#	#	开启电源供应器	ON					
Public	EEPROM Check	#	#	#	100	AOC	#	#	检查EEPROM	Custom	QSFP10-56G_AOC-Pactech	C-Temp			
Public	Version	#	#	#	100	AOC	#	All	检查产品版本	Custom	QSFP10_*_AOC_*_V02*_EFM8LB1.hex		AOC			
TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	开启 Burn-in Mode	Enable					
Public	Threshold Check	#	#	#	#	AOC	#	All	检查阀值						
TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	关闭 Burn-in Mode	Disable					
Public	Read Temp	#	#	#	0	AOC	#	#	读取产品温度				
Public	EEPROM BIN Dump	#	#	#	10	Report	#	#	存取EEPROM BIN档案	EEPROM Dump\4_QSFP10 56G AOC Pactech EEPROM Update
Public	EEPROM Dump	#	#	#	10	Report	#	#	存取EEPROM CSV档案			
Public	Set Station	#	#	#	0	AOC	#	#	写入产品测试状态	3					
None	Close	#	#	#	0	#	#	#	-------关闭程序-------		
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	QSFP10 56G AOC Pactech EEPROM Update	Sync				
Public	Move Out	#	#	#	0	MES	#	#	数据上传MES系统	EEPROM		#DEBUG	4		数据上传MES系统失败,MES回传错误讯息为 : %s	数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常
Public	Error Code	#	#	#	0	AOC	#	All	取得错误码						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	关闭USB-I2C		
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪器				
Public	Set Remain Count	#	#	#	0	MES	#	#	测板寿命扣減	测板寿命已用尽，请更换
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败				
