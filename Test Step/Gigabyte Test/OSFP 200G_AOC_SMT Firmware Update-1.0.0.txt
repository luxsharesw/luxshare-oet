Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	Multi FW Update	#	0	#	0	Test Item	#	All	Initialize Table	QSFPDD	Gigabyte				
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	Gigabyte PCB Test				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Firmware Update	#	#	#	0	COB	#	#	Update FW	OSFP 200G	80000	#Disable			
Public	Voltage3.3	10	3.4	3.1	0	COB	#	#	Check Voltage3.3						
#Public	Voltage1.8	10	1.85	1.75	0	COB	#	#	Check Voltage1.8						
Public	SMT Lot Number	#	#	#	0	COB	#	#	Set SMT Lot Number						
Public	Get SMT Lot Number	#	#	#	0	COB	#	#	Get SMT Lot Number						
Public	Check SMT Lot Number	#	#	#	0	COB	#	#	Check SMT Lot Number						
Public	Firmware Update	#	#	#	0	Report	#	#	Report	OSFP 200G_AOC_SMT		SMT Lot Number			
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result	COB	
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect										
End	#	#	#	#	0	#	#	#	Test Done						
