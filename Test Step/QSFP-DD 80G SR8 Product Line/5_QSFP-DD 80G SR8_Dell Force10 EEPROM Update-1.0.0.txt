Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	SR8 EEPROM	#	0	#	0	Test Item	#	ALL	Initialize Table
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	AOC EEPROM CH1	
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON							
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP-DD				
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get PN & SN	#	#	#	#	Module	#	#	Get PN & SN						
Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	EEPROM Update	#	#	#	100	Module	#	#	EEPROM Update	Custom	QSFP-DD 80G_SR8-Dell Force10	C-Temp
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON			
Public	EEPROM Check	3	#	#	100	Module	#	#	EEPROM Check	Custom	QSFP-DD 80G_SR8-Dell Force10	C-Temp			
Public	Version	#	#	#	#	AOC	#	All	Version Check	Custom	QSFP-DD 80G_*_SR8_*_V03*.hex	SR8	
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Enable Burn-in Mode	Enable	
Public	Threshold Check	3	#	#	#	AOC	#	All	Threshold Check	0					
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Disable Burn-in Mode	Disable		
Public	Report CSV	#	#	#	0	Report	#	#	Report	5_QSFP-DD 80G SR8_Dell Force10 EEPROM Update	Sync				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect							
End	#	#	#	#	0	#	#	#	Test Done						
