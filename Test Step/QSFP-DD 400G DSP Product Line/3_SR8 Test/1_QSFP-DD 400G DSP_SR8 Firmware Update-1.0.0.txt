Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0	
Public	PAM4 DSP Firmware Update	#	0	#	0	Test Item	#	NONE	Initialize Table							
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------							
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info							
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply						
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF						
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP					
Public	Set Power State	#	#	#	2000	Hardware	#	#	Power ON	ON						
Public	Wait Current By Work Order	#	2.6	0.9	4000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current			Firmware Update
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	QSFP-DD						
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	0	TW					
None	Start	#	#	#	0	#	#	#	-------Test Step-------							
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number							
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info							
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber							
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line							
Public	Firmware Update By Work Order	#	#	#	100	Product	#	#	Firmware Update		120000				
Public	Get Version Info	#	#	#	#	Product	#	All	Version Check	Work Order					
Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Check DSP							
Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA							
Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	#	"3,3.5"	0.68	#			
Public	Set Analog Vcc	1	#	#	100	PAM4 DSP	#	#	Set Analog Vcc 750mV	750						
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	1	TW					
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage							
None	Close	#	#	#	0	#	#	#	-------Close Step-------							
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record							
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP-DD 400G DSP Product Line\3_SR8 Test\1_QSFP-DD 400G DSP_SR8 Firmware Update						
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP-DD 400G DSP Product Line\3_SR8 Test\1_QSFP-DD 400G DSP_SR8 Firmware Update	Lot Number		Result			
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 400G DSP Product Line\3_SR8 Test	Result					
Public	Close Product	#	#	#	0	Product	#	#	Close USB-I2C							
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF						
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect							
End	#	#	#	#	0	#	#	#	Test Done							
