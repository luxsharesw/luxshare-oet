Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC EEPROM	#	0	#	0	Test Item	#	ALL	Initialize Table	QSFPDD					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP DC				
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP-DD				
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage						
Public	Set InitMode	#	#	#	#	PAM4 DSP	#	All	Enable SW InitMode	Enable					
Public	Set MSA Optional	#	#	#	#	PAM4 DSP	#	All	MSA Optional to Arista	Arista					
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get PN & SN	#	#	#	#	Module	#	#	Get PN & SN						
#Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	Get Product Temp Define											
Public	EEPROM Update	#	#	#	#	Module	#	#	EEPROM Update	Custom	QSFP-DD 400G DSP_AOC-Arista	C-Temp	Ethernet CMIS 4.1		
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
Public	EEPROM Check	3	#	#	#	Module	#	#	EEPROM Check	Custom	QSFP-DD 400G DSP_AOC-Arista	C-Temp	Ethernet CMIS 4.1		
Public	Version	#	#	#	#	AOC	#	All	Version Check	Custom	QSFP-DD 400G DSP_*_AOC-Arista_*_V04*_EFM8LB1.hex				
Public	Calibration DDMI By Product	#	#	#	100	Product	#	All	Calibration Temp	Temp					
Public	Set Voltage Slope	#	#	#	100	Product	#	#	Set Voltage Slope	132	7				
#Public	Threshold Check	#	#	#	#	AOC	#	All	Threshold Check	0					
Public	Upload Product EEPROM	#	#	#	0	Cloud	#	#	Upload EEPROM Data						
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage						
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP-DD 400G DSP Product Line\3_AOC Test\EEPROM Update\QSFP-DD DSP AOC 4.1 One Side_Arista EEPROM Update					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP-DD 400G DSP Product Line\3_AOC Test\EEPROM Update\QSFP-DD DSP AOC 4.1 One Side_Arista EEPROM Update	EEPROM				
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 400G DSP Product Line\3_AOC Test\EEPROM Update\QSFP-DD DSP AOC 4.1 One Side_Arista EEPROM Update		Disable User Folder					
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	100	AOC	#	#	Close USB-I2C						
#Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
#Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
#Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
End	#	#	#	#	0	#	#	#	Test Done						
