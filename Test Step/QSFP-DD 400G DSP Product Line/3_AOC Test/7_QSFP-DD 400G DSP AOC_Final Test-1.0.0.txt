Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Final Test	#	0	#	0	Test Item	#	8	Initialize Table	AOC					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info						
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT	16				
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 FEC Test				
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP				
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON					
Public	Wait Current	#	6.6	4	1000	Hardware	#	#	Wait DSP Current	10	100	10	Start Current		
Public	Sort BERT 59281	#	#	#	0	Hardware	#	#	Sort BERT						
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Search Product By Identifier	#	#	#	100	Module	#	#	Search Product Type	QSFP-DD,QSFP-DD	50	50	QSFP-DD 400G DSP			
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage						
Public	Get Part Number	#	#	#	0	Product	#	#	Get Part Number	All					
Public	Get Serial Number	#	#	#	0	Product	#	#	Get Serial Number	All					
Public	Get Length	#	#	#	0	Product	#	#	Get Length					
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line							
None	Start	#	#	#	0	#	#	#	-------Check Info-------						
Public	Check Threshold Value	#	#	#	100	Product	#	#	Check TxP Threshold Value	Tx Power	Check DDMI	0			
Public	Check Threshold Value	#	#	#	100	Product	#	#	Check RxP Threshold Value	Rx Power	Check DDMI	1			
Public	Check Threshold Value	#	#	#	100	Product	#	#	Check Tx Bias Threshold Value	Tx Bias	Check DDMI	2			
Public	Check Threshold Value	#	#	#	100	Product	#	#	Check Temp Threshold Value	Temp	Check DDMI	3			
Public	Check Threshold Value	#	#	#	100	Product	#	#	Check Voltage Threshold Value	Voltage	Check DDMI	4			
Public	Get DDMI	#	1	-1	100	Product	#	#	Verify Tx Power	Tx Power	0		Whole Data		
Public	Get DDMI	#	0	-2	100	Product	#	#	Verify Rx Power	Rx Power	0		Whole Data		
Public	Get DDMI	#	8	6	100	Product	#	#	Verify Tx Bias	Tx Bias	0		Whole Data		
Public	Get DDMI	#	40	10	100	Product	#	#	Verify Temp	Temperature	0				
Public	Get DDMI	#	3.4	3.1	100	Product	#	#	Verify Voltage	Voltage	0				
None	Start	#	#	#	0	#	#	#	-------BER Test-------						
Public	Relock	#	#	#	4000	Hardware	#	#	BERT Relock						
Public	Get PAM4 FEC	#	4	0	#	Hardware	#	2	Get FEC Data	80	5.00E-08		Multi Product		
Public	Threshold Check	3	#	#	#	AOC	#	All	Threshold Check	1	CHECK	#INTL Assert			
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	6.6	5	#	Hardware	#	1	Get Current
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record	Final Test							
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP-DD 400G DSP Product Line\3_AOC Test\7_QSFP-DD 400G DSP AOC_Final Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP-DD 400G DSP Product Line\3_AOC Test\7_QSFP-DD 400G DSP AOC_Final Test	Final Test	Result			
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 400G DSP Product Line\3_AOC Test	Result
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
