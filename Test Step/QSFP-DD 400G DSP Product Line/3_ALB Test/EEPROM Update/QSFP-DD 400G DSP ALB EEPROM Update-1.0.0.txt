Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC EEPROM	#	0	#	0	Test Item	#	ALL	Initialize Table	QSFPDD ALB					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP DC				
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP-DD
Public	Check Station	#	#	#	0	AOC	#	#	Check Station State	4	TW
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage									
Public	Get PN & SN	#	#	#	#	Module	#	#	Get PN & SN
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info		
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber					
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line						
Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	Get Product Temp Define				
#Public	Set InitMode	#	#	#	#	PAM4 DSP	#	All	Enable SW InitMode	Enable
Public	Default Slope	#	#	#	100	Product	#	#	Default Voltage Slope	Voltage				
Public	Default Slope	#	#	#	100	Product	#	#	Default Temp Slope	Temp
None	Start	#	#	#	0	#	#	#	-------Test Step-------
Public	Calibration DDMI By Product	#	#	#	#	Product	100	All	Calibration Temp	Temp	ALB		
Public	EEPROM Update By Work Order	#	#	#	#	Module	#	#	EEPROM Update
Public	Serial Number Check	#	#	#	#	Product	#	#	Check Serial Number				
Public	Get DDMI	#	3.4	3.10	100	Product	#	#	Verify Voltage	Voltage	0				
Public	Get DDMI	#	40	10	100	Product	#	#	Verify Temp	Temperature	0
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON
None	None	#	#	#	0	#	#	#	-------Check Status Step-------						
Public	EEPROM Check By Work Order	#	#	#	#	Module	#	#	EEPROM Check		
Public	Get Version Info	#	#	#	#	Product	#	All	Version Check	Work Order	
Public	Check DDMI By Threshold	#	#	#	100	Product	#	#	Check DDMI Value	ALB EEPROM	1			
Public	Get DDMI	#	40	10	100	Product	#	#	Verify Temp	Temperature	1
Public	Get DDMI	#	3.4	3.1	100	Product	#	#	Verify Voltage	Voltage	1
Public	Check USER EEPROM NVR	#	3.456	3.135	0	Product	#	#	Check VCC	VCC					
Public	Check USER EEPROM NVR	#	3.456	3.135	0	Product	#	#	Check VCC2	VCC2					
Public	Check USER EEPROM NVR	#	3.456	3.135	0	Product	#	#	Check VCCTX	VCCTX					
Public	Check USER EEPROM NVR	#	3.456	3.135	0	Product	#	#	Check VCCRX	VCCRX				
Public	Check USER EEPROM NVR	#	3.456	3.135	0	Product	#	#	Check VCCTX1	VCCTX1				
Public	Check USER EEPROM NVR	#	3.456	3.135	0	Product	#	#	Check VCCRX2	VCCRX1									
Public	Threshold Check	3	#	#	#	AOC	#	All	Threshold Check	2	ALB EEPROM	INTL Assert
Public	EEPROM Dump	#	#	#	#	Report	#	#	Dump EEPROM Data						
Public	Upload Product EEPROM	#	#	#	0	Cloud	#	#	Upload EEPROM Data
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record
Public	Record PNSN	#	#	#	0	Work Order	#	#	Edit Workorder PN SN Record						
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP-DD 400G DSP Product Line\3_ALB Test\EEPROM Update\QSFP-DD 400G DSP ALB EEPROM Update					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP-DD 400G DSP Product Line\3_ALB Test\EEPROM Update\QSFP-DD 400G DSP ALB EEPROM Update	EEPROM				
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 400G DSP Product Line\3_ALB Test	Result			
Public	Set Station	#	#	#	0	AOC	#	#	Set Test State	5	TW				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Close USB-I2C	#	#	#	100	AOC	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
End	#	#	#	#	0	#	#	#	Test Done						
