Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Burn In Test	#	0	#	0	Test Item	#	8	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP				
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON					
Public	Wait Current	#	1.5	1	1000	Hardware	#	#	Wait DSP Current	5	100	10	Start Current	3	
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Meter	Power Meter					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Meter	Power Meter	QSFP-DD 400G				
None	Start	#	#	#	0	#	#	#	----------Test Step----------						
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	QSFP-DD					
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number						
#Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Get DSP Info						
#Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
#Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	#	"3.2,3.4"	0.67	#		
Public	Set Bias	#	#	#	100	VCSEL Driver	#	#	Set Bias	6.8	Single	Bias Current			
#Public	Wait Temp Stable	#	80	30	0	PAM4 DSP	#	#	Wait Temp Stable	1	1.5	1.5	600		
Public	Get DDMI	#	70	20	0	Product	#	#	Get MCU Temp	Temperature		MCU Temp			
None	Start	#	#	#	0	#	#	#	---------CH1 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	1	CH1 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	1	Get CH1 Power	Tx Power		2			
None	Start	#	#	#	0	#	#	#	---------CH2 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	2	CH2 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	2	Get CH2 Power	Tx Power		2			
None	Start	#	#	#	0	#	#	#	---------CH3 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	3	CH3 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	3	Get CH3 Power	Tx Power		2			
None	Start	#	#	#	0	#	#	#	---------CH4 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	4	CH4 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	4	Get CH4 Power	Tx Power		2			
None	Start	#	#	#	0	#	#	#	---------CH5 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	5	CH5 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	5	Get CH5 Power	Tx Power		2			
None	Start	#	#	#	0	#	#	#	---------CH6 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	6	CH6 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	6	Get CH6 Power	Tx Power		2			
None	Start	#	#	#	0	#	#	#	---------CH7 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	7	CH7 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	7	Get CH7 Power	Tx Power		2			
None	Start	#	#	#	0	#	#	#	---------CH8 Power----------						
Public	Set Power Down	#	#	#	500	VCSEL Driver	#	8	CH8 Tx Power Up	Disable					
Public	Get Power	3	#			Hardware	5	8	Get CH8 Power	Tx Power		2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	All	Set Tx Power On	Disable					
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	1.5	1	#	Hardware	#	1	Get Current						
Public	Report CSV	#	#	#	0	Report	#	#	Report	SMC Test\QSFP-DD 400G DSP\1_QSFP-DD 400G DSP DDMI Calibration_BI SMC Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SMC Test\QSFP-DD 400G DSP\1_QSFP-DD 400G DSP DDMI Calibration_BI SMC Test	Lot Number		Result		
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	SMC Test\QSFP-DD 400G DSP\1_QSFP-DD 400G DSP DDMI Calibration_BI SMC Test		Disable User Folder			
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
