Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0	
Public	AOC LIV Test	#	0	#	0	Test Item	#	1	初始化表格	SFP28	Post					
None	Init	#	#	#	0	#	#	#	-------初始程序-------							
Public	Connect	#	#	#	100	Cloud	#	#	Connect to Server	DG Server						
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply						
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	100G TRx CH1					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH2	Power Supply	100G TRx CH2					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH3	Power Supply	100G TRx CH3					
Public	Set Power State	#	#	#	1000	Hardware	#	#	开启电源供应器	ON				SFP10		
Public	Search OutSourcing Product	#	#	#	100	AOC	#	#	搜寻产品	SFP28		#Auto Reset Table				
Public	Write Outsourcing Password	#	#	#	1000	AOC	#	#	写入密码	SFP10	123					
Public	OutSourcing Write Lot Number	#	#	#	1000	AOC	#	#	Copy Lot Number							
Public	OutSourcing Get Lot Number	#	#	#	0	AOC	#	#	Get Lot Number	A2	CableSN							
Public	Input Parameter	#	#	#	0	MES	#	#	确认MES系统参数	FALSE						
Public	Move In	#	#	#	0	MES	#	#	访问MES系统	3	#DEBUG	"访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s"	"无法访问MES系统,请确认网路或MES连线状态"	"MES系统没有回应,请确认网路或MES连线状态"		
Public	Get Service Info	#	#	#	0	MES	#	#	确认测板寿命	测板寿命已用尽，请更换	#DEBUG	
None	Start	#	#	#	0	#	#	#	-------测试程序-------							
None	#	#	#	#	0	#	#	#	-------电示坡器程序-------							
Public	OutSourcing Check RSSI	#	50000	2500	100	AOC	10		Get RSSI	RSSI						
Public	Get Pre Test RSSI	#	#	#	100	AOC	#	#	Get PreTest RSSI	SFP28 AOC Ruijie Pre Burn-In Test	Lot Number		DG Server			
Public	Compare RSSI	#	1000	#	100	AOC	#	#	Get Variation	RSSI		Cali	dbm	1						
None	Close	#	#	#	0	#	#	#	-------关闭程序-------							
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	SFP28 AOC Ruijie Post Burn-In Test						
Public	Rename Current CSV	#	#	#	0	Report	#	#	存取测试报告	SFP28 AOC Ruijie Post Burn-In Test	TwoProduct							
Public	Move Out	#	#	#	0	MES	#	#	数据上传MES系统			OutSourcing	2		"数据上传MES系统失败,MES回传错误讯息为 : %s"	"数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常"
Public	Column Status	#	#	#	0	Function	#	#	设置表格	AOC						
Public	Error Code	#	#	#	0	AOC	#	All	取得错误码							
Public	Close USB-I2C	#	#	#	100	AOC	#	#	关闭USB-I2C							
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF						
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪器							
Public	Set Remain Count	#	#	#	0	MES	#	#	测板寿命扣減	测板寿命已用尽，请更换						
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败	请注意！ ！测试板寿命即将用尽，请准备！ ！目前剩余次数为	#DEBUG1			
