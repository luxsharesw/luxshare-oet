Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC HT BER	#	0	#	0	Test Item	#	None	初始化表格						
None	Init	#	#	#	0	#	#	#	-------初始程序-------						
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	100G TRx CH1				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH2	Power Supply	100G TRx CH2				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH3	Power Supply	100G TRx CH3				
Public	Set Power State	#	#	#	1000	Hardware	#	#	开启电源供应器	ON					
Public	Search Product	#	#	#	100	AOC	#	#	搜寻产品	BER		#Auto Reset Table	QSFP28 Fanout
Public	Verify Lot Code By PN	#	#	#	0	AOC	#	#	 比对产品内码	TRx	
Public	Input Parameter	#	#	#	0	MES	#	#	确认MES系统参数	False					
Public	Move In	#	#	#	0	MES	#	#	访问MES系统	2	#DEBUG	访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s	无法访问MES系统,请确认网路或MES连线状态	MES系统没有回应,请确认网路或MES连线状态
Public	Get Service Info	#	#	#	0	MES	#	#	确认测板寿命	测板寿命已用尽，请更换	#DEBUG
Public	Connect	3	#	#	100	Hardware	#	#	连接误码仪	BERT	8
Public	Configure	3	#	#	100	Hardware	#	#	设置误码仪	BERT								
None	Start	#	#	#	0	#	#	#	-------测试程序-------										
Public	Set Station	#	#	#	100	AOC	#	#	写入产品测试状态	1					
Public	Target Temp	3	85	65	100	AOC	#	All	确认产品温度	360		BER			
Public	Get Voltage	#	#	#	100	AOC	#	#	读取产品电压			BER								
Public	Get Error Data	0	#	#	0	Hardware	#	#	误码仪测试										
Public	Set Station	#	#	#	100	AOC	#	#	写入产品测试状态	2				
None	Close	#	#	#	0	#	#	#	-------关闭程序-------		
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	QSFP28 Fanout High Temp BER Test	Sync					
Public	Move Out	#	#	#	0	MES	#	#	数据上传MES系统			#DEBUG	3		数据上传MES系统失败,MES回传错误讯息为 : %s	数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常
Public	Column Status	#	#	#	0	Function	#	#	设置表格	AOC					
Public	Error Code	#	#	#	0	AOC	#	All	取得错误码	
Public	Close USB-I2C	#	#	#	100	AOC	#	#	关闭USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪
Public	Set Remain Count	#	#	#	0	MES	#	#	测板寿命扣減	测板寿命已用尽，请更换器						
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败				
