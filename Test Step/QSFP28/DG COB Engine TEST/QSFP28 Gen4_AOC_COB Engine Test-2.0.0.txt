Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V2.0.0
Public	COB QSFP Engine Test	#	0	#	0	Test Item	#	1	Initialize Table		Sync				
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
#Public	Configure	#	#	#	0	Hardware	#	#	Do Nothing						
#Public	Configure	#	#	#	0	Hardware	#	#	Do Nothing						
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Search COB Engine Test	#	#	#	#	COB	#	#	Search Product Type	COB Engine Test
#Public	Input Parameter	#	#	#	0	MES	#	#	Check MES Parameter	FALSE						
#Public	Move In	#	#	#	0	MES	#	#	MES Move In	6	COB	"访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s"	"无法访问MES系统,请确认网路或MES连线状态"	"MES系统没有回应,请确认网路或MES连线状态"				
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	QSFP28 COB Engine Test
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	DG Server					
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	#	#	#	0	Hardware	#	#	Configure BERT	BERT	QSFP28 AOC
None	#	#	#	#	0	#	#	#	-------Test Step-------						
#TRx Device	Set CDR Bypass	#	#	#	0	VCSEL Driver	#	All	Tx CDR Bypass	Enable	MSA				
#TRx Device	Set CDR Bypass	#	#	#	0	TIA Device	#	All	Rx CDR Bypass	Enable	MSA				
TRx Device	Set Burn-in	#	#	#	200	VCSEL Driver	#	All	Enable Burn-in Mode	Enable					
TRx Device	Set Burn-in Current	#	#	#	100	VCSEL Driver	#	All	Set Burn-in Current	7					
Public	Get Monitors	#	15000	5000	0	Module	#	All	Get Monitors						
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Disable Burn-in Mode	Disable					
Public	Target Temp	3	85	80	100	AOC	#	All	Wait Temp Stable	360		BER			
Public	Get Error Data	0	#	#	0	Hardware	#	#	Check Error Count	QSFP28		Module			
Public	Get Current	#	0.6	0.46	#	Hardware	#	1	Get Current	1
#Public	Move Out	#	90000	4000	0	MES	#	#	MES Move Out			COB	5		"数据上传MES系统失败,MES回传错误讯息为 : %s"	"数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常"		
Public	Get Computer Name	#	#	#	#	COB	#	#	Get Computer Name						
Public	Target Item	#	#	#	100	Report	#	#	Report CSV File	5_QSFP28 Gen4_AOC-COB Engine Test	Lot Number		Channel		
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	QSFP28 Gen4_AOC Product Line\5_QSFP28 Gen4_AOC-COB Engine Test					
Public	Arrange By LotCode	#	#	#	#	Report	#	#	Copy Report to LotCode Folder						
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	Test Done						
