Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	Set Burn-In and Copy Lotnum	#	0	#	0	Test Item	#	2	初始化表格						
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP28				
Public	Set Power State	#	#	#	2000	Hardware	#	#	Power ON	ON					
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	QSFP28	QSFP28				
Public	Get Lot Number	#	#	#	10	COB	#	#	Get Lot Number						
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Enable Burn-in Mode	Enable					
TRx Device	Set Burn-in Current	#	#	#	100	VCSEL Driver	#	All	Set Burn-in Current	4					
Public	Copy Lot Number to SN	#	#	#	100	COB	#	#	Lot Number to SN	Disable					
Public	Get Current	#	0	0	#	Hardware	#	1	Get Current	3					
Public	Set Power State	#	#	#	2000	Hardware	#	#	Power ON	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	測試結束						
