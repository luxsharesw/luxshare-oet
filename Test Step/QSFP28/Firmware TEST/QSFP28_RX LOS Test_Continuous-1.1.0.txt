Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.1.0					
Public	RX LOS	#	#	#	0	Test Item	#	All	初始化表格	QSFP28										
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------											
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP28									
None	Start	#	#	#	0	#	#	#	-------Test Step-------											
None	#	#	#	#	#	#	#	#	-------Tx Disable-------											
Public	Set GPIO	1	#	#	0	GPIO	#	All	設定ModSel/LowPowMode	0	1	1	0	0	0	0	0			
Public	Set Tx Disable	#	#	#	100	Firmware Test	#	#	設定Tx Disable	ON										
Public	Get Rx Los	#	#	#	0	Firmware Test	#	#	確認RX LOS	ON	0									
Public	Get INTL	#	#	#	0	Firmware Test	#	All	確認INTL	OFF	0									
Public	Get GPIO	1	#	#	0	GPIO	#	All	確認HW INTL	0	OFF	HW INTL								
Public	Get Tx Power	#	-Inf	-Inf	0	Firmware Test	#	#	取得TxP數據	0										
Public	Get Rx Power	#	-40	-40	0	Firmware Test	#	#	取得RxP數據	0										
Public	Get Tx Bias	#	0	0	0	Firmware Test	#	#	取得Tx Bias數據	0										
None	#	#	#	#	#	#	#	#	---------Mask---------											
Public	Mask All Flag	#	#	#	100	Firmware Test	#	All	設定Mask	T										
Public	Get INTL	#	#	#	0	Firmware Test	#	All	確認INTL	ON	1									
Public	Get GPIO	1	#	#	0	GPIO	#	All	確認HW INTL	0	ON	HW INTL								
Public	Mask All Flag	#	#	#	0	Firmware Test	#	All	取消Mask	F										
None	#	#	#	#	#	#	#	#	---------Tx Enable---------											
Public	Set Tx Disable	#	#	#	100	Firmware Test	#	#	設定Tx Disable	OFF										
Public	Get Rx Los	#	#	#	0	Firmware Test	#	#	確認RX LOS	OFF	1									
Public	Get Tx Fault	#	#	#	0	Firmware Test	#	All	確認Tx Fault	OFF	1									
Public	Get INTL	#	#	#	0	Firmware Test	#	#	確認INTL	ON	2									
Public	Get GPIO	1	#	#	0	GPIO	#	All	確認HW INTL	0	ON	HW INTL								
Public	Get Tx Power	#	100	-12	0	Firmware Test	#	#	取得TxP數據	1										
Public	Get Rx Power	#	100	-12	0	Firmware Test	#	#	取得RxP數據	1										
Public	Get Tx Bias	#	10	0.001	0	Firmware Test	#	#	取得Tx Bias數據	1										
None	#	#	#	#	#	#	#	#	-------Use Offset-------											
Public	Setting Calibration Data	#	#	#	0	Firmware Test	#	#	Offset DDMI											
Public	Get Rx Los	#	#	#	0	Firmware Test	#	#	確認RX LOS	ON	2									
Public	Get Tx Fault	#	#	#	0	Firmware Test	#	All	確認Tx Fault	OFF	2									
Public	Get INTL	#	#	#	0	Firmware Test	#	#	確認INTL	OFF	3									
Public	Get GPIO	1	#	#	0	GPIO	#	All	確認HW INTL	0	OFF	HW INTL								
Public	Get Tx Power	#	-Inf	-Inf	0	Firmware Test	#	#	取得TxP數據	2										
Public	Get Rx Power	#	-40	-40	0	Firmware Test	#	#	取得RxP數據	2										
Public	Get Tx Bias	#	0.003	0	0	Firmware Test	#	#	取得Tx Bias數據	2										
None	#	#	#	#	#	#	#	#	---------Mask---------											
Public	Mask All Flag	#	#	#	100	Firmware Test	#	All	設定Mask	T										
Public	Get INTL	#	#	#	0	Firmware Test	#	All	確認INTL	ON	4									
Public	Get GPIO	1	#	#	0	GPIO	#	All	確認HW INTL	0	ON	HW INTL								
Public	Mask All Flag	#	#	#	0	Firmware Test	#	All	取消Mask	F										
None	#	#	#	#	#	#	#	#	-----LP Mode High-----											
Public	Setting Calibration Data	#	#	#	0	Firmware Test	#	#	Default Calibration	Default Cal										
Public	Set GPIO	1	#	#	50	GPIO	#	All	設定LowPowMode	0	1	0	0	0	0	0	0			
Public	Get Rx Power	#	-40	-40	0	Firmware Test	#	#	取得RxP數據	3										
Public	Report CSV	#	#	#	0	Report	#	#	存取數據	QSFP28-RX LOS	Sync									
Public	Test Again	-1	#	0	0	Function	#	#	重複測試	Start										
None	Close	#	#	#	#	#	#	#	-------Close Step-------											
Public	Set GPIO	1	#	#	0	GPIO	#	All	設定ModSel/LowPowMode	0	1	1	0	0	0	0	0			
Public	Setting Calibration Data	#	#	#	0	Firmware Test	#	#	Default Calibration	Default Cal										
Public	Close USB-I2C	#	#	#	0	Module	#	#	Close USB-I2C											
End	#	#	#	#	#	#	#	#	測試結束											
