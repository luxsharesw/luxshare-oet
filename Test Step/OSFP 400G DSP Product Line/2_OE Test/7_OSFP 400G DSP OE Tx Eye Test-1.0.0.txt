Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Tx Eye Tune	#	0	#	0	Test Item	#	8	Initialize Table	Tx Tune					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 Tx Eye Tune				
Public	Connect	3	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM				
Public	Connect	3	#	#	0	Hardware	#	#	Connect RF Switch	RF Switch					
Public	Select A Channel	#	#	#	0	Hardware	#	#	Select Clock Tx 1-4						
Public	Connect	3	#	#	100	Hardware	#	#	Connect Scope	Scope					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Scope	Scope	QSFP-DD PAM4 DSP Tx Test				
Public	Wait Current	#	#	#	200	Hardware	#	#	Wait DSP Current	10	100	10	Idle Current		400G PAM4 DSP AOC
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	OSFP
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage									
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number						
None	Start	#	#	#	0	#	#	#	----------Test Step----------	
Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Get DSP Info					
Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	#	"3,3.3"	0.68	#				
Public	Set Analog Vcc	1	#	#	100	PAM4 DSP	#	#	Set Analog Vcc 750mV	750					
#Public	Wait Temp Stable	#	95	50	0	PAM4 DSP	#	#	Wait Temp Stable	10	2	2	600		
Public	Get DDMI	#	80	30	100	Product	#	#	Get MCU Temp	Temperature		MCU Temp			
None	#	#	#	#	0	#	#	#	-------------CH1-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	1	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	1	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	100	PAM4 DSP	#	1	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	1	Switch to CH1						
Public	Patterns	3	#	#	100	Hardware	#	#	CH1 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	1	CH1 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH1 - Verify Data	400G	PAM4 DSP OE							
Public	Save Screen	3	#	#	0	Hardware	#	1	CH1 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
None	#	#	#	#	0	#	#	#	-------------CH2-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	2	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	2	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	100	PAM4 DSP	#	2	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	2	Switch to CH2						
Public	Patterns	3	#	#	100	Hardware	#	#	CH2 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	2	CH2 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH2 - Verify Data	400G	PAM4 DSP OE								
Public	Save Screen	3	#	#	0	Hardware	#	2	CH2 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
None	#	#	#	#	0	#	#	#	-------------CH3-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	3	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	3	Get Temp	MCU Temp					
Public	Get DSP Temp	#	95	30	100	PAM4 DSP	#	3	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	3	Switch to CH3						
Public	Patterns	3	#	#	100	Hardware	#	#	CH3 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	3	CH3 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH3 - Verify Data	400G	PAM4 DSP OE					
Public	Save Screen	3	#	#	0	Hardware	#	3	CH3 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
None	#	#	#	#	0	#	#	#	-------------CH4-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	4	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	4	Get Temp	MCU Temp					
Public	Get DSP Temp	#	95	30	100	PAM4 DSP	#	4	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	4	Switch to CH4						
Public	Patterns	3	#	#	100	Hardware	#	#	CH4 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	4	CH4 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH4 - Verify Data	400G	PAM4 DSP OE						
Public	Save Screen	3	#	#	0	Hardware	#	4	CH4 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
Public	Select B Channel	5	#	#	1000	Hardware	#	#	Select Clock Tx 5-8						
None	#	#	#	#	0	#	#	#	-------------CH5-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	5	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	5	Get Temp	MCU Temp					
Public	Get DSP Temp	#	95	30	100	PAM4 DSP	#	5	Get DSP Temp	240					
Public	Switch Channel	#	#	#	1000	Hardware	#	5	Switch to CH5						
Public	Patterns	3	#	#	100	Hardware	#	#	CH5 - Measurement Data												
Public	Get PAM4 Data	#	#	#	0	Hardware	#	5	CH5 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH5 - Verify Data	400G	PAM4 DSP OE							
Public	Save Screen	3	#	#	0	Hardware	#	5	CH5 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
None	#	#	#	#	0	#	#	#	-------------CH6-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	6	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	6	Get Temp	MCU Temp					
Public	Get DSP Temp	#	95	30	100	PAM4 DSP	#	6	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	6	Switch to CH6						
Public	Patterns	3	#	#	100	Hardware	#	#	CH6 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	6	CH6 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH6 - Verify Data	400G	PAM4 DSP OE								
Public	Save Screen	3	#	#	0	Hardware	#	6	CH6 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
None	#	#	#	#	0	#	#	#	-------------CH7-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	7	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	7	Get Temp	MCU Temp					
Public	Get DSP Temp	#	95	30	100	PAM4 DSP	#	7	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	77 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	7	CH7 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH7 - Verify Data	400G	PAM4 DSP OE								
Public	Save Screen	3	#	#	0	Hardware	#	7	CH7 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
None	#	#	#	#	0	#	#	#	-------------CH8-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	8	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	8	Get Temp	MCU Temp					
Public	Get DSP Temp	#	95	30	100	PAM4 DSP	#	8	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	8	Switch to CH8						
Public	Patterns	3	#	#	100	Hardware	#	#	CH8 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	8	CH8 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH8 - Verify Data	400G	PAM4 DSP OE						
Public	Save Screen	3	#	#	0	Hardware	#	8	CH8 - Save Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test				
Public	Get Tx Info	1	#	#	100	PAM4 DSP	#	#	Get Line Side Data	Line-Side					
Public	Get Driver Data	#	#	#	100	VCSEL Driver	#	#	Get Vcsel Driver Data
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	#	#	#	Hardware	#	1	Get Current			400G PAM4 DSP AOC
Public	Report CSV	#	#	#	0	Report	#	#	Report	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test	Lot Number		Result	
Public	Copy Report By Lotnumber	#	#	#	0	Report	#	#	Copy Report	OSFP 400G DSP Product Line\2_OE Test
Public	Rename Eye Diagram Folder	#	#	#	0	Report	#	#	Rename Eye Diagram Folder	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test	Result				
Public	Copy Eye Diagram	#	#	#	0	Report	#	#	Copy Eye Diagram	OSFP 400G DSP Product Line\2_OE Test\7_OSFP 400G DSP OE Tx Eye Test	Result	Lotnumber					
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
