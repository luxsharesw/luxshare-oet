Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP High Temp BER Test	#	0	#	0	Test Item	#	8	Initialize Table					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info	
Public	Connect	2	#	#	100	Hardware	#	#	Connect Thermostream	Thermostream
Public	Configure	2	#	#	100	Hardware	#	#	Configure Thermostream	Thermostream	DSP 59281 Product Line
Public	Set Target Temp	2	#	#	0	Hardware	#	#	Set Target Temp	60	
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	OSFP PAM4 FEC Test
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	OSFP 400G DSP				
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON	
Public	Wait Current	#	1	0.7	200	Hardware	#	#	Wait DSP Current	7	100	10	Start Current		
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	OSFP
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber								
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line								
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	2	TW
Public	Set Data Path Power	#	#	#	4000	PAM4 DSP	#	1	Power Up	Enable				
None	Start	#	#	#	0	#	#	#	-------Test Step-------
Public	Get DSP Status	1	#	#	0	PAM4 DSP	#	#	Get DSP Info						
Public	Get Chip Status	#	#	#	0	PAM4 DSP	#	#	Check Vcsel Driver & TIA					
Public	Get ADC	1	#	#	0	PAM4 DSP	#	#	Get ADC	#	3.2,3.4	0.67	#
Public	Get Driver Data	#	#	#	0	VCSEL Driver	#	#	Get Vcsel Driver Data	Single	
Public	Get TIA Data	#	#	#	0	TIA Device	#	#	Get TIA Data	Single	
Public	Default Slope	#	#	#	0	Product	#	#	Default Rx Power Slope	Rx Power
Public	Wait Temp Stable	#	90	70	0	PAM4 DSP	#	#	Wait Temp Stable	10	2	2	600
Public	Get DDMI	#	85	60	0	Product	#	#	Get MCU Temp	Temperature		MCU Temp		
Public	Get DDMI	3	15000	3000	0	Product	#	#	Get RSSI	RSSI	0		Column
Public	Relock	#	#	#	4000	Hardware	#	#	BERT Relock
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Disable MCU DDMI	Disable	
Public	Get System Side Input	#	#	#	100	PAM4 DSP	#	#	Get System Side Input	30	5.00E-08	1	Column	Sync			
Public	Get PAM4 FEC	#	4	0	#	Hardware	#	2	Get FEC Data	120	5.00E-08	2	Column
Public	Wait Sync	#	#	#	0	Function	#	#	Wait Get System Side Input Finish	FEC Test
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable							
Public	Get Temp	#	#	#	100	PAM4 DSP	#	1	Get Final MCU Temp	MCU Final Temp							
Public	Get DSP Temp	#	96	50	200	PAM4 DSP	#	1	Get DSP Final Temp	1	DSP Final Temp							
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	3	TW
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage
None	Close	#	#	#	0	#	#	#	-------Close Step-------
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record	
Public	Get Current	#	3	2.65	#	Hardware	#	1	Get Current
Public	Report CSV	#	#	#	0	Report	#	#	Report	OSFP 400G DSP Product Line\3_SR8 Test\3_OSFP 400G DSP AOC_High Temp BER Test
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	OSFP 400G DSP Product Line\3_SR8 Test\3_OSFP 400G DSP AOC_High Temp BER Test	Lot Number		Result
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	OSFP 400G DSP Product Line\3_SR8 Test	Result			
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C	
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect													
End	#	#	#	#	0	#	#	#	Test Done						
