Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP BER Test	#	0	#	0	Test Item	#	8	Initialize Table
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	OSFP 400G DSP
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON
Public	Connect	2	#	#	100	Hardware	#	#	Connect Thermostream	Thermostream					
Public	Configure	2	#	#	100	Hardware	#	#	Configure Thermostream	Thermostream	DSP 59281 Product Line				
Public	Set Target Temp	2	#	#	0	Hardware	#	#	Set Target Temp	25				
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 FEC Test		
Public	Wait Current	#	2	0.7	1000	Hardware	#	#	Wait DSP Current	5	100	10	Start Current
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	OSFP
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber					
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line
Public	Set Data Path Power	#	#	#	4000	PAM4 DSP	#	1	Power Up	Enable									
None	Start	#	#	#	0	#	#	#	-------Test Step-------
Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Get DSP Info
Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA
Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	#	3.2,3.4	0.65	#
Public	Get Driver Data	#	#	#	100	VCSEL Driver	#	#	Get Vcsel Driver Data		Column	8
Public	Get TIA Data	#	#	#	100	TIA Device	#	#	Get TIA Data		Column	8	5
Public	Default Slope	#	#	#	0	Product	#	#	Default Rx Power Slope	Rx Power
Public	Wait Temp Stable	#	60	40	0	PAM4 DSP	#	#	Wait Temp Stable	10	2	2	600
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable
Public	Get DDMI	#	50	30	0	Product	#	#	Get MCU Temp	Temperature		MCU Temp
Public	Get DDMI	3	28000	4000	0	Product	#	#	Get RSSI	RSSI	9		#Column
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Disable MCU DDMI	Disable
Public	Relock	#	#	#	2000	Hardware	#	#	BERT Relock
Public	Get PAM4 FEC	#	4	0	#	Hardware	#	2	Get FEC Data	120	5.00E-08	2	Column	
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	6	TW
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage
None	Close	#	#	#	0	#	#	#	-------Close Step-------
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record
Public	Get Current	#	2.8	2.3	#	Hardware	#	1	Get Current
Public	Report CSV	#	#	#	0	Report	#	#	Report	OSFP 400G DSP Product Line\3_SR8 Test\5_OSFP 400G DSP SR8_BER Test
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	OSFP 400G DSP Product Line\3_SR8 Test\5_OSFP 400G DSP SR8_BER Test			Result
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	OSFP 400G DSP Product Line\3_AOC Test	Result
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
