Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC EEPROM	#	0	#	0	Test Item	#	ALL	Initialize Table	QSFPDD					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	OSFP 400G DSP				
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		OSFP
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage						
Public	Get PN & SN	#	#	#	#	Module	#	#	Get PN & SN						
Public	Check Product	#	#	#	#	Module	#	#	Check PN Type						
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info						
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber						
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line						
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	6	TW				
Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	Get Product Temp Define						
Public	Verify PN Temp Define	#	#	#	0	AOC	#	#	Verify Temp Define						
Public	Set InitMode	#	#	#	#	PAM4 DSP	#	All	Enable SW InitMode	Enable					
Public	Default Slope	#	#	#	100	Product	#	#	Default Voltage Slope	Voltage					
Public	Default Slope	#	#	#	100	Product	#	#	Default Temp Slope	Temp
None	Start	#	#	#	0	#	#	#	-------Test Step-------
Public	Calibration DDMI By Product	#	#	#	#	Product	100	All	Calibration Temp	Temp							
Public	EEPROM Update By Work Order	#	#	#	#	Module	#	#	EEPROM Update
Public	Serial Number Check	#	#	#	#	Product	#	#	Check Serial Number						
Public	Get DDMI	#	3.4	3.1	100	Product	#	#	Verify Voltage	Voltage	0				
Public	Get DDMI	#	40	10	100	Product	#	#	Verify Temp	Temperature	0				Ethernet CMIS 4.0		
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON
None	None	#	#	#	0	#	#	#	-------Check Status Step-------					
Public	EEPROM Check By Work Order	#	#	#	#	Module	#	#	EEPROM Check				Ethernet CMIS 4.0
Public	Get Version Info	#	#	#	#	Product	#	All	Version Check	Work Order					
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable	#8000					
Public	Check DDMI By Threshold	#	#	#	100	Product	#	#	Check DDMI Value	Module EEPROM	0				
Public	Get DDMI	#	3.4	3.1	100	Product	#	#	Verify Voltage	Voltage	1				
Public	Get DDMI	#	40	10	100	Product	#	#	Verify Temp	Temperature	1				
Public	Get DDMI	#	8	6	100	Product	#	#	Verify Tx Bias	Tx Bias	0		Whole Data		
Public	Get DDMI Calibration Data	#	#	#	100	Product	#	#	Verify Tx Power By Calibration Data	Tx Power	1	EEPROM	Work Order		
Public	Get DDMI Calibration Data	#	#	#	100	Product	#	#	Verify Rx Power By Calibration Data	Rx Power	2	EEPROM	Work Order		
Public	Threshold Check	3	#	#	#	AOC	#	All	Threshold Check	1	MODULE EEPROM	INTL Assert		
Public	EEPROM Dump	#	#	#	#	Report	#	#	Dump EEPROM Data							
Public	Upload Product EEPROM	#	#	#	0	Cloud	#	#	Upload EEPROM Data						
Public	Report CSV	#	#	#	0	Report	#	#	Report	OSFP 400G DSP Product Line\3_SR8 Test\EEPROM Update\OSFP DSP SR8 4.0_EEPROM Update
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	OSFP 400G DSP Product Line\3_SR8 Test\EEPROM Update\OSFP DSP SR8 4.0_EEPROM Update	EEPROM
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 400G DSP Product Line\3_SR8 Test	Result		Disable User Folder		
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
End	#	#	#	#	0	#	#	#	Test Done						
