Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	SR8 EEPROM	#	0	#	0	Test Item	#	ALL	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	AOC EEPROM CH1				
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON					
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP-DD				
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Wait MCU Temp	#	#	#	100	Product	#	#	Wait MCU Temp	50	20				
Public	Threshold Check	#	#	#	#	AOC	#	All	Threshold Check						
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP-DD 200G Product Line\2_QSFP-DD 200G_Room Temp BER Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP-DD 200G Product Line\2_QSFP-DD 200G_Room Temp BER Test	Lot Number				
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 200G Product Line\2_QSFP-DD 200G_Room Temp BER Test		Disable User Folder			
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
