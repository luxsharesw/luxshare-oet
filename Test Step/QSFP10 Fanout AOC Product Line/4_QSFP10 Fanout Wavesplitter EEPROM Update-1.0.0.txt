Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC EEPROM Update	#	0	#	0	Test Item	#	NONE	初始化表格						
None	Init	#	#	#	0	#	#	#	-------初始程序-------										
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	AOC EEPROM CH1				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH2	Power Supply	AOC EEPROM CH2				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH3	Power Supply	AOC EEPROM CH3				
Public	Set Power State	#	#	#	500	Hardware	#	#	开启电源供应器	ON					
None	Start	#	#	#	0	#	#	#	-------测试程序-------						
Public	Search QSFP10 Fanout	#	#	#	0	AOC	#	#	搜寻产品
Public	Check Product ID	#	#	#	0	AOC	#	#	确认产品型号	EEPROM
Public	Get 40G Fanout Lot Number	#	#	#	0	AOC	#	#	讀取產品內碼
Public	Input Parameter	#	#	#	0	MES	#	#	确认MES系统参数	False					
Public	Move In	#	#	#	0	MES	#	#	访问MES系统	3	#DEBUG	访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s	无法访问MES系统,请确认网路或MES连线状态	MES系统没有回应,请确认网路或MES连线状态
Public	Get Service Info	#	#	#	0	MES	#	#	确认测板寿命	测板寿命已用尽，请更换	#DEBUG	
Public	Write Outsourcing Password	#	#	#	1000	AOC	#	#	写入密码	SFP10	123					
Public	EEPROM Update	#	#	#	100	AOC	#	#	更新EEPROM	Custom	QSFP10 Fanout_AOC-Wavesplitter	C-Temp
Public	Set Power State	#	#	#	500	Hardware	#	#	关闭电源供应器	OFF					
Public	Set Power State	#	#	#	500	Hardware	#	#	开启电源供应器	ON					
Public	EEPROM Check	#	#	#	100	AOC	#	#	检查EEPROM	Custom	QSFP10 Fanout_AOC-Wavesplitter	C-Temp
Public	Check A2 Checksum	#	#	#	100	AOC	#	#	Check Page A2 Checksum	
Public	Version	#	#	#	100	AOC	#	All	检查产品版本	#Custom	QSFP10_*_AOC_*_V02*_EFM8LB1.hex		QSFP10 Fanout
TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	开启 Burn-in Mode	Enable
TRx Device	Set Burn-in Current	#	#	#	0	VCSEL Driver	#	All	Set Burn-in Current	6			
Public	Threshold Check	#	#	#	#	AOC	#	All	检查阀值	
TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	关闭 Burn-in Mode	Disable	
Public	EEPROM BIN Dump	#	#	#	10	Report	#	#	存取EEPROM BIN档案	EEPROM Dump\4_QSFP10 Fanout Wavesplitter EEPROM Update
Public	EEPROM Dump	#	#	#	10	Report	#	#	存取EEPROM CSV档案		
Public	Read Temp	#	#	#	0	AOC	#	#	读取产品温度									
None	Close	#	#	#	0	#	#	#	-------关闭程序-------		
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	QSFP10 Fanout Wavesplitter EEPROM Update	Sync				
Public	Move Out	#	#	#	0	MES	#	#	数据上传MES系统	EEPROM		#DEBUG	4		数据上传MES系统失败,MES回传错误讯息为 : %s	数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常
Public	Error Code	#	#	#	0	AOC	#	All	取得错误码						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	关闭USB-I2C		
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪器					
Public	Set Remain Count	#	#	#	0	MES	#	#	测板寿命扣減	测板寿命已用尽，请更换	
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败				
