Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.5
Public	LIV Pre-Test	#	0	#	0	Test Item	#	All	初始化表格	QSFP-DD 200G	0.5	LIV Pre Test	8.5	6
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
#Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP-DD 200G				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Search Product Type	#	#	#	10	COB	#	#	Search Product Type	LIV Pre Test					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP-DD 200G				
TRx Device	Set Power Down	#	#	#	0	VCSEL Driver	#	All	Tx Power Down	Enable					
Public	Get Lot Number	#	#	#	10	COB	#	#	Get Lot Number						
Public	Download WID File	#	#	#	0	Cloud	#	#	Get WID Number						
Public	WID Number	#	#	#	10	COB	#	#	Write WID Number						
Public	Get WID Number	3	#	#	10	COB	#	#	Check WID Number						
None	#	#	#	#	0	#	#	#	-------Tx Setting-------						
TRx Device	Set Burn-in	#	#	#	10	VCSEL Driver	#	All	Enable Burn-in Mode	Enable					
None	#	#	#	#	0	#	#	#	-------LIV Curve-------						
Public	LIV Curve	#	24000	1000	0	COB	#	#	Get LIV Curve	QSFP-DD 200G	0				
Public	Calc SE	#	2500	700	0	COB	#	#	Calc SE/ITH	0					
TRx Device	Set Power Down	#	#	#	10	VCSEL Driver	#	All	Tx Power Up	Disable					
Public	Get Current	#	0	0	#	Hardware	#	1	Get Current	3					
Public	LIV Pre Test	#	#	#	0	Report	#	#	Report	2_QSFP-DD 200G_AOC-LIV Pre Test					
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 200G_AOC Product Line\2_QSFP-DD 200G_AOC-LIV Pre Test					
Public	Merge Pre Test File	#	#	#	0	COB	#	#	Merge Test File	\\123.51.249.53\Test_Report\QSFP-DD 200G_AOC Product Line\2_QSFP-DD 200G_AOC-LIV Pre Test					
Public	Upload	#	#	#	0	Cloud	#	#	Upload Test Data	Dropbox	2_QSFP-DD 200G_AOC-LIV Pre Test				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result	COB	
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C					
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect											
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
End	#	#	#	#	0	#	#	#	測試結束						
