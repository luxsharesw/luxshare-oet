Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	RSSI Test	#	0	#	0	Test Item	#	8	初始化表格						
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP-DD				
Public	Get RSSI	3	15000	3000	#	Module	#	#	Get RSSI	50					
Public	Report CSV	#	#	#	500	Report	#	#	存取數據	5_QSFP-DD 400G DSP_SR8-RSSI Test					
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 400G DSP_SR8 Product Line\5_QSFP-DD 400G DSP_SR8-RSSI Test					
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	測試結束						
