Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.2
Public	PAM4 LIV Pre-Test	#	0	#	0	Test Item	#	All	初始化表格	8					
None	Start	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP-DD 400G				
Public	Set Power State	#	#	#	100	Hardware	#	#	Power ON	On					
Public	Wait Current	#	2.2	1.5	1000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current		
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	QSFP-DD					
#Public	Search Product Type	#	#	#	#	COB	#	#	Search Product Type	LIV Pre Test					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP-DD 400G				
None	#	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get ID Info	#	#	#	0	Product	#	#	Get Lot Number						
Public	Get ADC	1	#	#	0	PAM4 DSP	#	#	Get ADC	#	"3.2,3.4"	0.65	#		
None	#	#	#	#	0	#	#	#	-------Set Burn-In-------						
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Enable Burn-in Mode	Enable	PAM4				
TRx Device	Set Burn-in Current	#	#	#	100	VCSEL Driver	#	All	Set Burn-in Current	7.5	PAM4				
None	#	#	#	#	0	#	#	#	-------Get TX Power-------						
Public	Set Power Down	#	#	#	2000	VCSEL Driver	#	All	All Channel Enable	Enable					
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	1	Tx 1 Disable	Disable					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Meter	Power Meter					
Public	Configure	#	#	#	100	Hardware	#	#	Configure Power Meter	Power Meter	QSFP-DD 400G				
Public	Get Power	#	#	#	0	Hardware	5	1	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	2	Tx 2 Disable	Disable					
Public	Get Power	#	#	#	0	Hardware	5	2	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	3	Tx 3 Disable	Disable					
Public	Get Power	#	#	#	0	Hardware	5	3	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	4	Tx 4 Disable	Disable					
Public	Get Power	#	#	#	0	Hardware	5	4	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	5	Tx 5 Disable	Disable					
Public	Get Power	#	#	#	0	Hardware	5	5	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	6	Tx 6 Disable	Disable					
Public	Get Power	#	#	#	0	Hardware	5	6	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	7	Tx 7 Disable	Disable					
Public	Get Power	#	#	#	0	Hardware	5	7	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	8	Tx 8 Disable	Disable					
Public	Get Power	#	#	#	0	Hardware	5	8	Get Power	TX Power	DBM	2			
Public	Set Power Down	#	#	#	100	VCSEL Driver	#	All	All Channel Disable	Disable					
Public	Set Burn-in Current	#	#	#	100	VCSEL Driver	#	All	Set Burn-in Current	7.5					
None	#	#	#	#	0	#	#	#	-------Test Report-------						
Public	Get Current	#	2.4	2	#	Hardware	#	1	Get Current	3					
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP-DD 400G DSP\4_QSFP-DD 400G DSP_SR8-LIV Pre Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP-DD 400G DSP\4_QSFP-DD 400G DSP_SR8-LIV Pre Test	Lot Number				
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	QSFP-DD 400G DSP\4_QSFP-DD 400G DSP_SR8-LIV Pre Test		Disable User Folder			
Public	Upload	#	#	#	0	Cloud	#	#	Upload Test Data	Dropbox	QSFP-DD 400G DSP\4_QSFP-DD 400G DSP_SR8-LIV Pre Test				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result	COB					
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C						
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
End	#	#	#	#	0	#	#	#	測試結束						
