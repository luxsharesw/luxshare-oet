Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0		
Public	Quick Software	#	#	#	0	Test Item	#	All	初始化表格								
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------								
Public	Set GPIO	1	#	#	100	GPIO	#	All	設定LPMode=1/ResetL=1	1	1	1	1	0	0	0	0
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP-DD							
None	Start	#	#	#	0	#	#	#	-------Test Step-------								
Public	Get GPIO	1	#	#	0	GPIO	#	1	確認LPMode Pin	0	HIGH	LPMode					
Public	Get GPIO	1	#	#	0	GPIO	#	1	確認ResetL Pin	1	HIGH	ResetL					
Public	Get GPIO	1	#	#	0	GPIO	#	1	確認ModPrsL Pin	2	LOW	ModPrsL					
Public	Get Module Action State	1	#	#	0	Firmware Test	#	1	Get Module State	ModuleLowPwr	DataPathDeactivated						
None	#	#	#	#	0	#	#	#	--------------1--------------								
Public	Get GPIO	5	#	#	100	GPIO	#	1	確認IntL Pin	3	LOW	IntL Pin					
Public	Get Module Action State	1	#	#	0	Firmware Test	#	2	Get Module State	ModuleLowPwr	DataPathDeactivated						
Public	Get Module State Changed	1	#	#	0	Firmware Test	#	1	Get Module State Changed								
Public	Get INTL	1	#	#	0	Firmware Test	#	#	Get IntL Flag	ON	0	1					
Public	Get GPIO	1	#	#	0	GPIO	#	2	確認IntL Pin	3	HIGH	IntL Pin					
Public	Set LowPwr	1	#	#	0	Firmware Test	#	#	Set LowPwr=0	0							
None	#	#	#	#	0	#	#	#	--------------2--------------								
Public	Get GPIO	5	#	#	100	GPIO	#	3	確認IntL Pin	3	LOW	IntL Pin					
Public	Get Module Action State	50	#	#	100	Firmware Test	#	3	Get Module State	ModuleReady	DataPathDeactivated						
Public	Get Module State Changed	1	#	#	0	Firmware Test	#	2	Get Module State Changed								
Public	Get INTL	1	#	#	0	Firmware Test	#	#	Get IntL Flag	ON	1	1					
Public	Get GPIO	1	#	#	0	GPIO	#	4	確認IntL Pin	3	HIGH	IntL Pin					
None	#	#	#	#	0	#	#	#	--------------3--------------								
Public	Get GPIO	10	#	#	100	GPIO	#	5	確認IntL Pin	3	LOW	IntL Pin					
Public	Get Module Action State	1	#	#	0	Firmware Test	#	4	Get Module State	ModuleReady	DataPathActivated						
Public	Get Data Path State Changed	1	#	#	0	Firmware Test	#	1	Get Data Path State Changed								
Public	Get INTL	1	#	#	0	Firmware Test	#	#	Get IntL Flag	ON	2	1					
Public	Get GPIO	1	#	#	0	GPIO	#	6	確認IntL Pin	3	HIGH	IntL Pin					
#Public	Report CSV	#	#	#	0	Report	#	#	存取數據	2_QSFP-DD 400G Quick Software Initialization	Sync						
None	Close	#	#	#	#	#	#	#	-------Close Step-------								
Public	Close USB-I2C	#	#	#	0	Module	#	#	Close USB-I2C								
End	#	#	#	#	#	#	#	#	測試結束								
