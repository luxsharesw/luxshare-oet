Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	CMIS4.0 EEPROM	#	0	#	0	Test Item	#	All	Initialize Table	QSFP-DD					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Open USB-I2C	#	0	#	0	Firmware Test	#	#	Open USB-I2C	QSFP-DD	50	100
None	Start	#	#	#	0	#	#	#	-------Test Step-------											
Public	CMIS4.0 Low Page Check	#	0	#	0	Firmware Test	#	#	Low Page EEPROM Check
Public	CMIS4.0 Page00 Check	#	0	#	0	Firmware Test	#	#	Page00 EEPROM Check
Public	CMIS4.0 Page01 Check	#	0	#	0	Firmware Test	#	#	Page01 EEPROM Check
Public	CMIS4.0 Page02 Check	#	0	#	0	Firmware Test	#	#	Page02 EEPROM Check
Public	CMIS4.0 Page10 Check	#	0	#	0	Firmware Test	#	#	Page10 EEPROM Check
Public	CMIS4.0 Page11 Check	#	0	#	0	Firmware Test	#	#	Page11 EEPROM Check
Public	Report CSV	#	#	#	0	Report	#	#	�s���ƾ�	QSFP-DD 400G CMIS4.0 EEPROM Check
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	0	#	0	Firmware Test	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	Test Done						
