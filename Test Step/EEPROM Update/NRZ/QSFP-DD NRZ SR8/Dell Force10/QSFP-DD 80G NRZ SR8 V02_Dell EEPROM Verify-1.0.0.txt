Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC EEPROM	#	0	#	0	Test Item	#	ALL	Initialize Table	QSFPDD					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP-DD				
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get PN & SN	#	#	#	#	Module	#	#	Get PN & SN						
Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	EEPROM Check	#	#	#	100	Module	#	#	EEPROM Check	Custom	QSFP-DD 80G_SR8-Dell Force10	C-Temp			
Public	Version	#	#	#	#	AOC	#	All	Version Check	Custom	QSFP-DD 80G_*_SR8_*_V02*.hex		SR8			
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP-DD 80G NRZ SR8 V02_Dell EEPROM Verify	Sync				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	Test Done						
