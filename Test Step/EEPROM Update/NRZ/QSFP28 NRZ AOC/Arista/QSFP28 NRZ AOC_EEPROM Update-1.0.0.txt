Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC Calibration&EEPROM	#	0	#	0	Test Item	#	All	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP DC				
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON				
#Public	Check PN Type	#	#	#	0	AOC	#	#	Check QR Code	AOC				
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Search Product	#	#	#	0	AOC	#	#	Search Product Type	OQC
Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	Get Product Temp Define						
Public	Verify PN Temp Define	#	#	#	0	AOC	#	#	Verify Temp Define						
Public	EEPROM Update	#	#	#	100	AOC	#	#	EEPROM Update	Custom	QSFP28_AOC	C-Temp
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON			
Public	EEPROM Check	#	#	#	100	AOC	#	#	EEPROM Check	Custom	QSFP28_AOC	C-Temp
Public	Version	#	#	#	100	AOC	#	All	Version	Custom	QSFP28_*_AOC_*_V02*_EFM8LB1.hex		
Public	Calculate Arista CRC32	#	#	#	100	AOC	#	#	Calculate Arista CRC32		
None	None	#	#	#	0	#	#	#	-------Calibration Step-------	
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Enable Burn-in Mode	Enable					
TRx Device	Set Burn-in Current	#	#	#	0	VCSEL Driver	#	All	Set Burn-in Current	7						
Public	Tx Bias Calibration	5	8	6	#	AOC	100	All	TX Bias Calibration	7	EEPROM				
Public	TxP Calibration	5	1	-1	#	AOC	100	All	TX Calibration	0	EEPROM				
Public	RxP Calibration	5	0	-2	#	AOC	100	All	RX Calibration	-1	EEPROM	
Public	Read Temp	#	#	#	0	AOC	#	#	Read Temp		
Public	Threshold Check	#	#	#	#	AOC	#	All	Threshold Check	1	
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Disable Burn-in Mode	Disable								
#Public	Serial Number Check	#	#	#	#	Product	#	#	Check Serial Number												
Public	Report CSV	#	#	#	0	Report	#	#	Report	EEPROM Update\QSFP28 NRZ AOC_EEPROM Update
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	EEPROM Update\QSFP28 NRZ AOC_EEPROM Update	EEPROM
#Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	EEPROM Update\QSFP28 NRZ AOC_EEPROM Update		Disable User Folder						
None	Close	#	#	#	0	#	#	#	-------Close Step-------											
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C		
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect								
End	#	#	#	#	0	#	#	#	Test Done						
