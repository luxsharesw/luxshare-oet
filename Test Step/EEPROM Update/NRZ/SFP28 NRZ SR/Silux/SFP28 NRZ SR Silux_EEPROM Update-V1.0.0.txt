Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	VV1.0.0
Public	SR EEPROM	#	0	#	0	Test Item	#	ALL	Initialize Table	SFP					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------	
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	AOC EEPROM CH1	
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON						
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type	0					
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get PN & SN	#	#	#	#	Module	#	#	Get PN & SN						
Public	Set Custom Password	#	#	#	#	Module	#	#	Set Custom Password	Silux					
Public	EEPROM Update	#	#	#	#	Module	#	#	EEPROM Update	Custom	SFP28_SR-Silux	C-Temp	
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON			
Public	EEPROM Check	#	#	#	#	Module	#	#	EEPROM Check	Custom	SFP28_SR-Silux	C-Temp			
Public	Version	#	#	#	#	AOC	#	All	Version Check				SR
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Enable Burn-in Mode	Enable	
TRx Device	Set Burn-in Current	#	#	#	100	VCSEL Driver	#	All	Set Burn-in Current	6					
Public	Threshold Check	#	#	#	#	AOC	#	All	Threshold Flag Check						
TRx Device	Set Burn-in	#	#	#	100	VCSEL Driver	#	All	Disable Burn-in Mode	Disable					
Public	Report CSV	#	#	#	0	Report	#	#	Report	EEPROM Update\SFP28 NRZ SR Silux_EEPROM Update
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	EEPROM Update\SFP28 NRZ SR Silux_EEPROM Update	EEPROM
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	EEPROM Update\SFP28 NRZ SR Silux_EEPROM Update		Disable User Folder	
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C	
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
