Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	Alarm Warning	#	0	#	0	Test Item	#	All	Initialize Table	QSFP28					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Open USB-I2C	#	0	#	0	Firmware Test	#	#	Open USB-I2C	QSFP56 CMIS	200	50		
Public	Default Threshold	#	0	#	100	Firmware Test	#	#	Default Threshold						
None	Start	#	#	#	0	#	#	#	-------Test Step-------				
None	#	#	#	#	0	#	#	#	---Temp Alarm Warning---						
Public	High Warning	#	0	#	0	Firmware Test	#	#	High Warning	Temp					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Warning Mask	Temp	High Warning				
Public	High Alarm	#	0	#	0	Firmware Test	#	#	High Alarm	Temp					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Alarm Mask	Temp	High Alarm				
Public	Low Warning	#	0	#	0	Firmware Test	#	#	Low Warning	Temp					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Warning Mask	Temp	Low Warning				
Public	Low Alarm	#	0	#	0	Firmware Test	#	#	Low Alarm	Temp					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Alarm Mask	Temp	Low Alarm				
Public	Normal Flag	#	0	#	0	Firmware Test	#	#	Temp	Temp					
None	#	#	#	#	0	#	#	#	---Vcc Alarm Warning---						
Public	High Warning	#	0	#	0	Firmware Test	#	#	High Warning	Voltage					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Warning Mask	Voltage	High Warning				
Public	High Alarm	#	0	#	0	Firmware Test	#	#	High Alarm	Voltage					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Alarm Mask	Voltage	High Alarm				
Public	Low Warning	#	0	#	0	Firmware Test	#	#	Low Warning	Voltage					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Warning Mask	Voltage	Low Warning				
Public	Low Alarm	#	0	#	0	Firmware Test	#	#	Low Alarm	Voltage					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Alarm Mask	Voltage	Low Alarm				
Public	Normal Flag	#	0	#	0	Firmware Test	#	#	Voltage	Voltage					
None	#	#	#	#	0	#	#	#	---Tx Bias Alarm Warning---						
Public	High Warning	#	0	#	3000	Firmware Test	#	#	High Warning	Tx Bias	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Warning Mask	Tx Bias	High Warning	#Skip
Public	High Alarm	#	0	#	3000	Firmware Test	#	#	High Alarm	Tx Bias	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Alarm Mask	Tx Bias	High Alarm	#Skip
Public	Low Warning	#	0	#	3000	Firmware Test	#	#	Low Warning	Tx Bias	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Warning Mask	Tx Bias	Low Warning	#Skip
Public	Low Alarm	#	0	#	3000	Firmware Test	#	#	Low Alarm	Tx Bias	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Alarm Mask	Tx Bias	Low Alarm	#Skip
Public	Normal Flag	#	0	#	3000	Firmware Test	#	#	Tx Bias	Tx Bias					
None	#	#	#	#	0	#	#	#	---TxP Alarm Warning---						
Public	High Warning	#	0	#	3000	Firmware Test	#	#	High Warning	Tx Power	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Warning Mask	Tx Power	High Warning	#Skip
Public	High Alarm	#	0	#	3000	Firmware Test	#	#	High Alarm	Tx Power	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Alarm Mask	Tx Power	High Alarm	#Skip
Public	Low Warning	#	0	#	3000	Firmware Test	#	#	Low Warning	Tx Power	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Warning Mask	Tx Power	Low Warning	#Skip
Public	Low Alarm	#	0	#	3000	Firmware Test	#	#	Low Alarm	Tx Power	#Skip
Public	Mask Flags	#	0	#	2000	Firmware Test	#	#	High Alarm Mask	Tx Power	Low Alarm	#Skip
Public	Normal Flag	#	0	#	3000	Firmware Test	#	#	Tx Power	Tx Power	#Skip				
None	#	#	#	#	0	#	#	#	---RxP Alarm Warning---						
Public	High Warning	#	0	#	0	Firmware Test	#	#	High Warning	Rx Power					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Warning Mask	Rx Power	High Warning				
Public	High Alarm	#	0	#	0	Firmware Test	#	#	High Alarm	Rx Power					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Alarm Mask	Rx Power	High Alarm				
Public	Low Warning	#	0	#	0	Firmware Test	#	#	Low Warning	Rx Power					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Warning Mask	Rx Power	Low Warning				
Public	Low Alarm	#	0	#	0	Firmware Test	#	#	Low Alarm	Rx Power					
Public	Mask Flags	#	0	#	100	Firmware Test	#	#	High Alarm Mask	Rx Power	Low Alarm				
Public	Normal Flag	#	0	#	0	Firmware Test	#	#	Rx Power	Rx Power					
Public	Setting Calibration Data	#	0	#	0	Firmware Test	#	#	Default Calibation	Default Cal					
Public	Report CSV	#	#	#	0	Report	#	#	�s���ƾ�	QSFP56 200G CMIS Alarm Warning					
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	0	#	0	Firmware Test	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	Test Done						
