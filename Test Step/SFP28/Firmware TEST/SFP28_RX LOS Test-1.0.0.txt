Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	RX LOS	#	#	#	0	Test Item	#	1	初始化表格						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		SFP28	
TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	Burn-in Mode Enable	Enable				
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
None	#	#	#	#	#	#	#	#	-------Tx Disable-------						
Public	Set Tx Disable	#	#	#	100	Firmware Test	#	#	設定Tx Disable	ON					
Public	Get Rx Los	#	#	#	0	Firmware Test	#	#	確認RX LOS	ON	0				
Public	Get Tx Power	#	-Inf	-Inf	0	Firmware Test	#	#	取得TxP數據	0					
Public	Get Rx Power	#	-40	-40	0	Firmware Test	#	#	取得RxP數據	0					
None	#	#	#	#	#	#	#	#	-------Tx Enable-------						
Public	Set Tx Disable	#	#	#	100	Firmware Test	#	#	設定Tx Disable	OFF					
Public	Get Rx Los	#	#	#	0	Firmware Test	#	#	確認RX LOS	OFF	1				
Public	Get Tx Power	#	#	#	0	Firmware Test	#	#	取得TxP數據	1					
Public	Get Rx Power	#	#	#	0	Firmware Test	#	#	取得RxP數據	1					
None	#	#	#	#	#	#	#	#	-------Use Offset-------						
Public	Setting Calibration Data	#	#	#	0	Firmware Test	#	#	Offset TRx Power						
Public	Get Rx Los	#	#	#	0	Firmware Test	#	#	確認RX LOS	ON	2				
Public	Get Tx Power	#	-Inf	-Inf	0	Firmware Test	#	#	取得TxP數據	2					
Public	Get Rx Power	#	-40	-40	0	Firmware Test	#	#	取得RxP數據	2					
None	#	#	#	#	#	#	#	#	-------Default Offset-------						
Public	Setting Calibration Data	#	#	#	0	Firmware Test	#	#	Default Calibration	Default Cal					
Public	Report CSV	#	#	#	0	Report	#	#	存取數據	SFP28-RX LOS					
None	Close	#	#	#	#	#	#	#	-------Close Step-------	
TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	Burn-in Mode Disable	Disable						
Public	Close USB-I2C	#	#	#	0	Module	#	#	Close USB-I2C						
End	#	#	#	#	#	#	#	#	測試結束						
