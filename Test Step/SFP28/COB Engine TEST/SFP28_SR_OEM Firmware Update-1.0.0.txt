Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	Multi FW Update	#	0	#	0	Test Item	#	All	Initialize Table	SFP28					
None	Start	#	#	#	0	#	#	#	-------Test Step-------	
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox						
#Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
#Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB SFP28				
#Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Firmware Update	#	#	#	100	COB	#	#	 Firmware Update	SFP28 OEM					
Public	Voltage3.3	10	3.6	3	0	COB	#	#	Check Voltage3.3						
Public	Voltage1.8	10	1.9	1.7	0	COB	#	#	Check Voltage1.8						
Public	Set Lot Number	#	#	#	100	COB	#	#	Set Lot Number						
Public	Get SMT Lot Number	#	#	#	100	COB	#	#	Get SMT Lot Number						
Public	Get Lot Number	#	#	#	100	COB	#	#	Get Lot Number						
Public	Lot Number To SN	#	#	#	100	COB	#	#	Lot Number To SN
Public	Download WID File	#	#	#	0	Cloud	#	#	Get WID Number							
None	#	#	#	#	0	#	#	#	-------Tx Setting-------						
#TRx Device	Set Burn-in	#	#	#	200	VCSEL Driver	#	All	Enable Burn-in Mode	Enable					
#TRx Device	Set Burn-in Current	#	#	#	100	VCSEL Driver	#	All	Set Burn-in Current	Server									
#Public	Get Current	#	0	0	#	Hardware	#	1	Get Current	3					
Public	Firmware Update	#	#	#	0	Report	#	#	Save Test Data	1_SFP28_SR-OEM Firmware Update										
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result	COB	
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C					
#Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox						
End	#	#	#	#	0	#	#	#	Test Done						
