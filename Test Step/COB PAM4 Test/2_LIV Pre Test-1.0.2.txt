Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.2
Public	PAM4 LIV Post-Test	#	0	#	0	Test Item	#	All	初始化表格	8					
None	Start	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP-DD 400G				
Public	Set Power State	#	#	#	100	Hardware	#	#	Power ON	On					
Public	Wait Current	#	2.2	1.5	1000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current		
Public	Search PAM4 product Type	#	#	#	#	COB	#	#	Search Product Type	LIV Pre Test
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result	COB					
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C						
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
End	#	#	#	#	0	#	#	#	測試結束						
