Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	Loopback Calibration	#	0	#	0	Test Item	#	4	Initialize Table	QSFP					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type		QSFP56 CMIS,QSFP-DD				
None	Start	#	#	#	0	#	#	#	-------Test Step-------									
Public	Skip Old Lot Number	#	#	#	100	Module	#	#	Scan Module	1947
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	Test Done						
