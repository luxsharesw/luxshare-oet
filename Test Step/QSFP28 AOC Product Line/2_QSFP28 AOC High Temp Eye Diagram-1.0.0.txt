Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC HT Eye Diagram	#	0	#	0	Test Item	#	All	初始化表格	Current					
None	Init	#	#	#	0	#	#	#	-------初始程序-------						
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	100G TRx CH1				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH2	Power Supply	100G TRx CH2				
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH3	Power Supply	100G TRx CH3				
Public	Set Power State	#	#	#	1000	Hardware	#	#	开启电源供应器	ON					
Public	Search Product	#	#	#	100	AOC	#	#	搜寻产品	HT Eye		#Auto Reset Table	QSFP28
Public	Verify Lot Code By PN	#	#	#	0	AOC	#	#	 比对产品内码	TRx	
Public	Input Parameter	#	#	#	0	MES	#	#	确认MES系统参数	False					
Public	Move In	#	#	#	0	MES	#	#	访问MES系统	1	#DEBUG	访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s	无法访问MES系统,请确认网路或MES连线状态	MES系统没有回应,请确认网路或MES连线状态
Public	Get Service Info	#	#	#	0	MES	#	#	确认测板寿命	测板寿命已用尽，请更换	#DEBUG									
Public	Connect	3	#	#	100	Hardware	#	#	连接电示坡器	Scope	
Public	Connect	3	#	#	100	Hardware	#	#	连接误码仪	BERT	8
Public	Configure	3	#	#	0	Hardware	#	#	设置误码仪	BERT					
None	Start	#	#	#	0	#	#	#	-------测试程序-------											
Public	Set Station	#	#	#	100	AOC	#	#	写入产品测试状态	0					
Public	Target Temp	3	85	65	100	AOC	#	All	确认产品温度	360					
Public	Get Voltage	#	#	#	100	AOC	#	#	读取产品电压										
None	#	#	#	#	0	#	#	#	-------校正程序-------						
Public	TxP Calibration	3	0	-2	100	AOC	#	All	校正发射光功率	-1					
Public	RxP Calibration	3	-1	-3	100	AOC	#	All	校正接收光功率	-2					
Public	Tx Bias Calibration	3	8	6	100	AOC	#	All	校正发射偏压	7					
None	#	#	#	#	0	#	#	#	-------电示坡器程序-------			
Public	Configure	#	#	#	100	Hardware	#	#	设置电示坡器	Scope	AOC							
Public	Waveforms	#	#	#	0	Hardware	#	All	电示坡器测试		Show				
Public	Get Measurement	#	#	#	0	Hardware	#	4	取得电示坡器数据	AOC					
Public	Verify Measurement	#	#	#	0	Hardware	#	#	验证电示坡器数据	AOC					
Public	Save Screen	#	#	#	0	Hardware	#	#	存取电眼图	AOC High Temp Eye Diagram	AOC		
Public	Get Current	#	0.65	0.45	0	Hardware	#	1	取得产品A电流	1		Eye Diagram
Public	Get Current	#	0.65	0.45	0	Hardware	#	2	取得产品B电流	2		Eye Diagram
Public	Set Station	#	#	#	100	AOC	#	#	写入产品测试状态	1				
None	Close	#	#	#	0	#	#	#	-------关闭程序-------		
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	QSFP28 AOC High Temp Eye Diagram	Sync					
Public	Move Out	#	#	#	0	MES	#	#	数据上传MES系统			#DEBUG	2		数据上传MES系统失败,MES回传错误讯息为 : %s	数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常
Public	Column Status	#	#	#	0	Function	#	#	设置表格	AOC					
Public	Error Code	#	#	#	0	AOC	#	All	取得错误码	
Public	Close USB-I2C	#	#	#	100	AOC	#	#	关闭USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪器	
Public	Set Remain Count	#	#	#	0	MES	#	#	测板寿命扣減	测板寿命已用尽，请更换						
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败	请注意！ ！测试板寿命即将用尽，请准备！ ！目前剩余次数为	#DEBUG1	
