Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0	
Public	AOC EEPROM Update	#	0	#	0	Test Item	#	1	初始化表格							
None	Init	#	#	#	0	#	#	#	-------初始程序-------							
#Public	Check PN Type	#	#	#	0	AOC	#	#	检查QR码	AOC						
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply						
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	AOC EEPROM CH1					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH2	Power Supply	AOC EEPROM CH2					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH3	Power Supply	AOC EEPROM CH3					
Public	Set Power State	#	#	#	1000	Hardware	#	#	开启电源供应器	ON						
None	Start	#	#	#	0	#	#	#	-------测试程序-------							
Public	Search OutSourcing Product	#	#	#	100	AOC	#	#	搜寻产品			#Auto Reset Table
Public	OutSourcing Get Lot Number	#	#	#	0	AOC	#	#	Get Lot Number	PNSN	
Public	Write Outsourcing Password	#	#	#	1000	AOC	#	#	写入密码	SFP10	123					
Public	Input Parameter	#	#	#	0	MES	#	#	确认MES系统参数	FALSE						
Public	Move In	#	#	#	0	MES	#	#	访问MES系统	4	#DEBUG	"访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s"	"无法访问MES系统,请确认网路或MES连线状态"	"MES系统没有回应,请确认网路或MES连线状态"		
#Public	Get Service Info	#	#	#	0	MES	#	#	确认测板寿命	测板寿命已用尽，请更换	#DEBUG					
#Public	Set Station	#	#	#	0	AOC	#	#	写入产品测试状态	1						
Public	EEPROM Check	3	#	#	100	AOC	#	#	检查EEPROM	Custom	SFP10_AOC	C-Temp				
#Public	Version	#	#	#	100	AOC	#	All	检查产品版本				AOC			
Public	Enable RSSI Read	#	10000	4000	100	AOC	#	#	Enable Read RSSI	Enable						
Public	Threshold Check	#	#	#	#	AOC	#	All	检查阀值	Custom	SFP10_AOC	C-Temp				
Public	Enable RSSI Read	#	10000	4000	100	AOC	#	#	Disable Read RSSI	Disable						
#Public	Set Station	#	#	#	0	AOC	#	#	写入产品测试状态	2						
None	Close	#	#	#	0	#	#	#	-------关闭程序-------							
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	SFP10 AOC EEPROM Check	Sync					
#Public	Check EEPROM Success	#	#	#	0	AOC	#	#	Check EEPROM Success	4_SFP10 AOC EEPROM Check	32					
Public	Move Out	#	#	#	0	MES	#	#	数据上传MES系统			#DEBUG	6		"数据上传MES系统失败,MES回传错误讯息为 : %s"	"数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常"
Public	Error Code	#	#	#	0	AOC	#	All	取得错误码							
Public	Close USB-I2C	#	#	#	0	AOC	#	#	关闭USB-I2C							
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF						
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪器							
#Public	Set Remain Count	#	#	#	0	MES	#	#	测板寿命扣減	测板寿命已用尽，请更换						
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败	请注意！ ！测试板寿命即将用尽，请准备！ ！目前剩余次数为	#DEBUG1			
