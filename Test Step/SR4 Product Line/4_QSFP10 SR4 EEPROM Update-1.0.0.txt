Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	SR4 EEPROM	#	0	#	0	Test Item	#	ALL	初始化表格						
None	Init	#	#	#	0	#	#	#	-------初始程序-------						
Public	Connect	#	#	#	0	Hardware	#	#	连接电源供应器	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	设置电源供应器 CH1	Power Supply	SR4 CH1				
Public	Set Power State	#	#	#	500	Hardware	#	#	开启电源供应器	ON					
None	Start	#	#	#	0	#	#	#	-------测试程序-------						
Public	Search Product	#	#	#	#	Module	#	#	搜寻产品		QSFP10	
Public	Input Parameter	#	#	#	0	MES	#	#	确认MES系统参数	False					
Public	Move In	#	#	#	0	MES	#	#	访问MES系统	4	#DEBUG	访问MES系统失败 : 请确认该工单<%s> , MES回传错误讯息为 : %s	无法访问MES系统,请确认网路或MES连线状态	MES系统没有回应,请确认网路或MES连线状态
Public	Get Service Info	#	#	#	0	MES	#	#	确认测板寿命	测板寿命已用尽，请更换	#DEBUG						
Public	Set Customer Password	#	#	#	0	AOC	#	#	写入客制密码									
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	取得产品温度范围						
Public	Verify PN Temp Define	#	#	#	0	AOC	#	#	确认产品温度范围						
#Public	Input Voltage Calibration	#	#	#	0	Hardware	#	#	校准输入电压值	3.3	5	0.5							
None	Start	#	#	#	0	#	#	#	-------更新EEPROM程序-------		
Public	Get PN & SN	#	#	#	#	Module	#	#	Get PN & SN						
Public	EEPROM Update	#	#	#	#	Module	#	#	更新EEPROM
Public	Set Power State	#	#	#	1000	Hardware	#	#	关闭电源供应器	OFF					
Public	Set Power State	#	#	#	1000	Hardware	#	#	开启电源供应器	ON										
Public	EEPROM Check	#	#	#	#	Module	#	#	确认EEPROM				
Public	Version	#	#	#	#	AOC	#	All	确认分位版本	Custom	QSFP10_*_SR4_*_*_EFM8LB1.hex		SR4
#TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	开启 Burn-in Mode	Enable					
#Public	Threshold Check	#	#	#	#	AOC	#	All	检查阀值						
#TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	关闭 Burn-in Mode	Disable		
Public	EEPROM BIN Dump	#	#	#	10	Report	#	#	存取EEPROM BIN档案	EEPROM Dump\4_QSFP10 SR4 EEPROM Update
Public	EEPROM Dump	#	#	#	10	Report	#	#	存取EEPROM CSV档案													
None	Close	#	#	#	0	#	#	#	-------关闭程序-------	
Public	Report CSV	#	#	#	0	Report	#	#	存取测试报告	QSFP10 SR4 EEPROM Update	Sync	
Public	Move Out	#	#	#	0	MES	#	#	数据上传MES系统	EEPROM		#DEBUG	7		数据上传MES系统失败,MES回传错误讯息为 : %s	数据上传MES系统异常,可能原因为1.该电脑网路已断线, 2.网路不稳定, 3.MES系统连线状态异常
Public	Close USB-I2C	#	#	#	0	AOC	#	#	关闭USB-I2C							
Public	Set Power State	#	#	#	0	Hardware	#	#	关闭电源供应器	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	关闭所有仪器	
Public	Set Remain Count	#	#	#	0	MES	#	#	测板寿命扣減	测板寿命已用尽，请更换						
End	#	#	#	#	0	#	#	#	测试结束	测试完成	测试失败	请注意！ ！测试板寿命即将用尽，请准备！ ！目前剩余次数为	#DEBUG1
