Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Tx Eye Tune	#	0	#	0	Test Item	#	8	Initialize Table	Tx Tune					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 Tx Eye Tune				
Public	Connect	3	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM										
Public	Connect	3	#	#	100	Hardware	#	#	Connect Scope	Scope					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Scope	Scope	QSFP-DD PAM4 DSP Tx Tune				
Public	Wait Current	#	2.65	2	200	Hardware	#	#	Wait DSP Current	10	100	10	Idle Current		
Public	Search Product	#	#	#	1000	Product	#	#	Search Product Type	QSFP56 SFF8636,QSFP56 CMIS					
Public	Get ID Info	#	#	#	0	Product	#	#	Get Lot Number						
None	Start	#	#	#	0	#	#	#	----------Test Step----------						
Public	Get DSP Status	1	#	#	0	PAM4 DSP	#	#	Get DSP Info						
Public	Get Chip Status	#	#	#	0	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	0	PAM4 DSP	#	#	Get ADC	#	"3,3.3"	0.68	#		
#Public	Wait Temp Stable	#	95	50	0	PAM4 DSP	#	#	Wait Temp Stable	10	2	2	600		
Public	Get DDMI	#	80	30	0	Product	#	#	Get MCU Temp	Temperature		MCU Temp			
None	#	#	#	#	0	#	#	#	-------------CH1-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	0	PAM4 DSP	#	1	Get Voltage	1					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	1	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	1	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	1	Switch to CH1						
Public	Patterns	3	#	#	100	Hardware	#	#	CH1 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	1	CH1 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH1 - Verify Data	400G	PAM4 DSP AOC				
None	#	#	#	#	0	#	#	#	-------------CH2-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	0	PAM4 DSP	#	2	Get Voltage	1					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	2	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	2	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	2	Switch to CH2						
Public	Patterns	3	#	#	100	Hardware	#	#	CH2 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	2	CH2 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH2 - Verify Data	400G	PAM4 DSP AOC				
None	#	#	#	#	0	#	#	#	-------------CH3-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	0	PAM4 DSP	#	3	Get Voltage	1					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	3	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	3	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	3	Switch to CH3						
Public	Patterns	3	#	#	100	Hardware	#	#	CH3 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	3	CH3 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH3 - Verify Data	400G	PAM4 DSP AOC				
None	#	#	#	#	0	#	#	#	-------------CH4-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	0	PAM4 DSP	#	4	Get Voltage	1					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	4	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	4	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	4	Switch to CH4						
Public	Patterns	3	#	#	100	Hardware	#	#	CH4 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	4	CH4 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH4 - Verify Data	400G	PAM4 DSP AOC								
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	2.65	2.2	#	Hardware	#	1	Get Current						
Public	Report CSV	#	#	#	0	Report	#	#	Report	SMC Test\QSFP56 200G DSP\3_QSFP56 200G DSP_Eye Tuning SMC Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SMC Test\QSFP56 200G DSP\3_QSFP56 200G DSP_Eye Tuning SMC Test	Lot Number		Result		
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	SMC Test\QSFP56 200G DSP\3_QSFP56 200G DSP_Eye Tuning SMC Test		Disable User Folder			
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
