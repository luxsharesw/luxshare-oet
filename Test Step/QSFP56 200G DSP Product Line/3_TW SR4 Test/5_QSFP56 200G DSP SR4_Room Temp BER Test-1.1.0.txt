Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.1.0
Public	PAM4 DSP Room Temp BER Test	#	0	#	0	Test Item	#	4	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info						
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 FEC Test				
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP56 200G DSP BERT 59281				
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON
Public	Connect	2	#	#	100	Hardware	#	#	Connect Thermostream	Thermostream					
Public	Configure	2	#	#	100	Hardware	#	#	Configure Thermostream	Thermostream	DSP 59241 Product Line				
Public	Set Target Temp	2	#	#	0	Hardware	#	#	Set Target Temp	25										
PPublic	Wait Current By Work Order	#	1.8	1.1	1000	Hardware	#	3	Wait DSP Current	5	100	10	Start Current		
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	QSFP56 SFF8636,QSFP56 CMIS
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage							
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber							
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	4	TW							
None	Start	#	#	#	0	#	#	#	-------Test Step-------
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable												
Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Get DSP Info						
Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	#	"3.2,3.4"	0.67	#		
Public	Get Driver Data	#	#	#	100	VCSEL Driver	#	#	Get Vcsel Driver Data						
Public	Get TIA Data	#	#	#	100	TIA Device	#	#	Get TIA Data	Single					
Public	Set System Side Tx Info	#	#	#	100	PAM4 DSP	#	#	Set System Side Tx Info	0	-12	136	0	0	0	
Public	Wait Temp Stable	#	55	20	100	PAM4 DSP	#	#	Wait Temp Stable	10	2	2	600		
Public	Get DDMI	#	50	20	100	Product	#	#	Get MCU Temp	Temperature		DDMI Temp			
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable				
Public	Default Slope	#	#	#	100	Product	#	#	Default Rx Power Slope	Rx Power					
Public	Get DDMI	3	15000	3500	100	Product	#	#	Get RSSI	RSSI	0		Column
Public	Relock	#	#	#	2000	Hardware	#	#	BERT Relock						
Public	Get PAM4 FEC	#	4	0	#	Hardware	#	2	Get FEC Data	120	5.00E-08	2	Column
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	1	Get Final MCU Temp	MCU Final Temp					
Public	Get DSP Temp	#	50	20	0	PAM4 DSP	#	1	Get DSP Final Temp	1	DSP Final Temp
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	5	TW		
None	Close	#	#	#	0	#	#	#	-------Close Step-------
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record						
Public	Get Current	#	#	#	#	Hardware	#	1	Get Current			200G PAM4 DSP AOC
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage					
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP56 200G DSP Product Line\3_TW SR4 Test\5_QSFP56 200G DSP SR4_Room Temp BER Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP56 200G DSP Product Line\3_TW SR4 Test\5_QSFP56 200G DSP SR4_Room Temp BER Test	Lot Number		Result		
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	QSFP56 200G DSP Product Line\3_TW SR4 Test	Result
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
