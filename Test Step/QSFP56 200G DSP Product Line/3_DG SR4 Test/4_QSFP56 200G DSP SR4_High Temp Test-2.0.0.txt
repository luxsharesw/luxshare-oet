Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V2.0.0
Public	Room Temp BERT Test 2.0	#	0	#	0	Test Item	#	4	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
#Public	Connect	2	#	#	100	Hardware	#	#	Connect Thermostream	Thermostream					
#Public	Configure	2	#	#	100	Hardware	#	#	Configure Thermostream	Thermostream	DSP 59241 Product Line				
#Public	Set Target Temp	2	#	#	0	Hardware	#	#	Set Target Temp	35					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP56 200G DSP CH1				
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP56 200G DSP CH2				
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP56 200G DSP CH3				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM				
Public	Connect	#	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM	Second			
Public	Sort Optical Switch	#	#	#	0	Hardware	#	#	Sort Optical Switch	4					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Meter	Power Meter					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Meter	Power Meter	QSFP56 200G DSP				
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	Q56 DG RT Test Step1				
Public	Connect	3	#	#	100	Hardware	#	#	Connect Scope	Scope					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Scope	Scope	QSFP-DD PAM4 DSP Tx Tune				
Public	Wait Current	#	1.7	1	200	Hardware	#	#	Wait DSP Current	7	100	10	Idle Current		
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	QSFP56 CMIS					
Public	Get ID Info	#	#	#	0	Product	#	#	Get Lot Number						
None	Start	#	#	#	0	#	#	#	----------Test Step----------						
Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Get DSP Info						
Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	#	"3.2,3.4"	0.65	#		
Public	Get Tx Info	1	#	#	0	PAM4 DSP	#	#	Get Line Side Data	Line-Side					
Public	Get Driver Data	#	#	#	100	VCSEL Driver	#	#	Get Vcsel Driver Data						
Public	Default Slope	#	#	#	100	Product	#	#	Default Rx Power Slope	Rx Power					
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get DDMI	3	15000	3000	100	Product	#	#	Get RSSI	RSSI	0		Column		
Public	Wait Temp Stable	#	80	50	0	PAM4 DSP	#	#	Wait Temp Stable	10	1.5	1.5	600		
None	#	#	#	#	0	#	#	#	-------------CH1-------------						
Public	Get Temp	#	#	#	0	PAM4 DSP	#	1	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	1	Get DSP Temp	240					
Public	Second Switch Channel	#	#	#	100	Hardware	#	1	Switch to DCA	1					
Public	Switch Channel	#	#	#	1000	Hardware	#	1	Switch to CH1						
Public	Patterns	3	#	#	100	Hardware	#	#	CH1 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	1	CH1 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH1 - Verify Data	200G	PAM4 DSP AOC				
Public	Save Screen	3	#	#	0	Hardware	#	1	CH1 - Save Eye Diagram	QSFP56 200G DSP Product Line\3_DG SR4 Test\4_QSFP56 200G DSP SR4_High Temp Test					
Public	Second Switch Channel	#	#	#	500	Hardware	#	2	Switch to Power Meter	2					
Public	Get Power	#	#	#	100	Hardware	#	1	Get Tx Power	Tx Power		0			
None	#	#	#	#	0	#	#	#	-------------CH2-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	2	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	2	Get DSP Temp	240					
Public	Second Switch Channel	#	#	#	100	Hardware	#	1	Switch to DCA	1					
Public	Switch Channel	#	#	#	1000	Hardware	#	2	Switch to CH2						
Public	Patterns	3	#	#	100	Hardware	#	#	CH2 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	2	CH2 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH2 - Verify Data	200G	PAM4 DSP AOC				
Public	Save Screen	3	#	#	0	Hardware	#	2	CH2 - Save Eye Diagram	QSFP56 200G DSP Product Line\3_DG SR4 Test\4_QSFP56 200G DSP SR4_High Temp Test					
Public	Second Switch Channel	#	#	#	500	Hardware	#	2	Switch to Power Meter	2					
Public	Get Power	#	#	#	100	Hardware	#	2	Get Tx Power	Tx Power		0			
None	#	#	#	#	0	#	#	#	-------------CH3-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	3	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	3	Get DSP Temp	240					
Public	Second Switch Channel	#	#	#	100	Hardware	#	1	Switch to DCA	1					
Public	Switch Channel	#	#	#	1000	Hardware	#	3	Switch to CH3						
Public	Patterns	3	#	#	100	Hardware	#	#	CH3 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	3	CH3 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH3 - Verify Data	200G	PAM4 DSP AOC				
Public	Save Screen	3	#	#	0	Hardware	#	3	CH3 - Save Eye Diagram	QSFP56 200G DSP Product Line\3_DG SR4 Test\4_QSFP56 200G DSP SR4_High Temp Test					
Public	Second Switch Channel	#	#	#	100	Hardware	#	2	Switch to Power Meter	2					
Public	Get Power	#	#	#	100	Hardware	#	3	Get Tx Power	Tx Power		0			
None	#	#	#	#	0	#	#	#	-------------CH4-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	4	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	0	PAM4 DSP	#	4	Get DSP Temp	240					
Public	Second Switch Channel	#	#	#	100	Hardware	#	1	Switch to DCA	1					
Public	Switch Channel	#	#	#	1000	Hardware	#	4	Switch to CH4						
Public	Patterns	3	#	#	100	Hardware	#	#	CH4 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	4	CH4 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH4 - Verify Data	200G	PAM4 DSP AOC				
Public	Save Screen	3	#	#	0	Hardware	#	4	CH4 - Save Eye Diagram	QSFP56 200G DSP Product Line\3_DG SR4 Test\4_QSFP56 200G DSP SR4_High Temp Test					
Public	Second Switch Channel	#	#	#	100	Hardware	#	2	Switch to Power Meter	2					
Public	Get Power	#	#	#	100	Hardware	#	4	Get Tx Power	Tx Power		0			
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	Q56 DG RT Test Step2				
Public	Relock	#	#	#	4000	Hardware	#	#	BERT Relock						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Disable MCU DDMI	Disable					
Public	Get System Side Input	#	#	#	100	PAM4 DSP	#	#	Get System Side Input	10	1.00E-08	1	Column		
Public	Get PAM4 FEC	#	5	0	#	Hardware	#	2	Get FEC Data	200	1.00E-07	2	Column		
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	1.7	1.2	#	Hardware	#	1	Get Current						
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP56 200G DSP Product Line\3_DG SR4 Test\4_QSFP56 200G DSP SR4_High Temp Test					
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
