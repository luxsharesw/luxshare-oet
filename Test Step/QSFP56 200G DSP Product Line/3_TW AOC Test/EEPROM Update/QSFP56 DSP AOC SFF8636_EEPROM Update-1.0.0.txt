Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC Calibration&EEPROM	#	0	#	0	Test Item	#	All	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	QSFP-DD 400G DSP DC				
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
Public	Search Product	#	#	#	100	AOC	#	#	Search Product Type	OQC					
Public	Check Voltage	#	3.3	3	100	Product	#	#	Check DDMI Voltage
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info							
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber							
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line						
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	6	TW				
Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	Get Product Temp Define						
Public	Verify PN Temp Define	#	#	#	0	AOC	#	#	Verify Temp Define				
Public	Default Slope	#	#	#	100	Product	#	#	Default Temp Slope	Temp					
Public	Default Slope	#	#	#	100	Product	#	#	Default Voltage Slope	Voltage					
Public	Default Slope	#	#	#	100	Product	#	#	Default Tx Bias Slope	Tx Bias					
Public	Default Slope	#	#	#	100	Product	#	#	Default Tx Power Slope	Tx Power					
Public	Default Slope	#	#	#	100	Product	#	#	Default Rx Power Slope	Rx Power					
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	EEPROM Update	#	#	#	100	AOC	#	#	EEPROM Update	Custom	QSFP56 SFF8636_AOC	C-Temp	Ethernet SFF8636
Public	Serial Number Check	#	#	#	#	Product	#	#	Check Serial Number						
None	None	#	#	#	0	#	#	#	-------Calibration Step-------
Public	Get DDMI	#	3.4	3.2	100	Product	#	#	Verify Voltage	Voltage	0				
Public	Calibration DDMI By Product	#	#	#	100	Product	#	#	Calibration Temp	Temp					
Public	Get DDMI	#	25	10	100	Product	#	#	Verify Temp	Temperature	0				
Public	Tx Bias Calibration	5	8	6	100	AOC	#	All	Tx Bias Calibration	7	EEPROM				
Public	TxP Calibration	5	1	-1	100	AOC	#	All	TX Calibration	0	EEPROM				
Public	RxP Calibration	5	0	-2	100	AOC	#	All	RX Calibration	-1	EEPROM				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
None	None	#	#	#	0	#	#	#	-------Check Status Step-------								
Public	EEPROM Check	#	#	#	100	AOC	#	#	EEPROM Check	Custom	QSFP56 SFF8636_AOC	C-Temp	Ethernet SFF8636
Public	Get Version Info	#	#	#	#	Product	#	All	Version Check		SFF8636
Public	Get Length	#	#	#	100	Product	#	#	Get Length						
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable		
Public	Check DDMI By Threshold	#	#	#	100	Product	#	#	Check DDMI Value		1				
Public	Get DDMI	#	3.4	3.2	100	Product	#	#	Verify Voltage	Voltage	1				
Public	Get DDMI	#	25	10	100	Product	#	#	Verify Temp	Temperature	1				
Public	Get DDMI	#	8	6	100	Product	#	#	Verify Tx Bias	Tx Bias	1		Whole Data		
Public	Get DDMI	#	1	-1	100	Product	#	#	Verify Tx Power	Tx Power	1		Whole Data		
Public	Get DDMI	#	0	-2	100	Product	#	#	Verify Rx Power	Rx Power	1		Whole Data		
Public	Threshold Check	3	#	#	100	AOC	#	All	Threshold Check	2	CHECK	#INTL Assert							
Public	EEPROM Dump	#	#	#	0	Report	#	#	Dump EEPROM Data						
Public	Upload Product EEPROM	#	#	#	0	Cloud	#	#	Upload EEPROM Data
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record
Public	Record PNSN	#	#	#	0	Work Order	#	#	Edit Workorder PN SN Record						
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP56 200G DSP Product Line\3_TW AOC Test\EEPROM Update\QSFP56 DSP AOC SFF8636_EEPROM Update
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP56 200G DSP Product Line\3_TW AOC Test\EEPROM Update\QSFP56 DSP AOC SFF8636_EEPROM Update	EEPROM
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	QSFP56 200G DSP Product Line\3_TW AOC Test	Result			
Public	Set Station	#	#	#	0	AOC	#	#	Set Test State	7	TW				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Close USB-I2C	#	#	#	100	AOC	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
End	#	#	#	#	0	#	#	#	Test Done						
