Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Firmware Update	#	0	#	0	Test Item	#	NONE	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP56 200G DSP				
Public	Set Power State	#	#	#	1500	Hardware	#	#	Power ON	ON					
Public	Wait Current	#	#	#	3000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current		200G PAM4 DSP AOC	Firmware Update	
Public	Search Product	#	#	#	1000	Product	#	#	Search Product Type	QSFP56 SFF8636,QSFP56 CMIS
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	0	TW						
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get ID Info	#	#	#	0	Product	#	#	Get Lot Number
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info							
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber							
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line													
Public	Firmware Update By Lot Number	#	#	#	100	Product	#	#	Firmware Update	AOC	120000		
Public	Get Version Info	#	#	#	#	Product	#	All	Version Check		AOC			
Public	Get DSP Status	1	#	#	0	PAM4 DSP	#	#	Check DSP						
Public	Get Chip Status	#	#	#	0	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	0	PAM4 DSP	#	#	Get ADC	#	"3.2,3.4"	0.65	#
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	1	TW
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage			
None	Close	#	#	#	0	#	#	#	-------Close Step-------
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record						
Public	Report CSV	#	#	#	0	Report	#	#	Report	QSFP56 200G DSP Product Line\3_TW AOC Test\1_QSFP56 200G DSP AOC_CMIS4.0 Firmware Update					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	QSFP56 200G DSP Product Line\3_TW AOC Test\1_QSFP56 200G DSP AOC_CMIS4.0 Firmware Update	Lot Number		Result		
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	QSFP56 200G DSP Product Line\3_TW AOC Test	Result	
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
