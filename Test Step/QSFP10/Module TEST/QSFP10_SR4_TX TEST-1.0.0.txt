Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	DCA	#	0	#	0	Test Item	#	All	初始化表格	QSFP10	Sync				
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Search Product	#	#	#	#	Module	#	#	Search Product Type						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Scope	Scope					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Scope	Scope	40G SR4				
Public	Connect	#	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM				
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
None	#	#	#	#	0	#	#	#	-------CH1-------						
Public	Switch Channel	#	#	#	0	Hardware	#	1	設定CH1						
Public	Get Temp	#	#	0	0	Firmware Test	#	#	取得Temp數據	0					
Public	Get Voltage	#	#	0	0	Firmware Test	#	#	取得Voltage數據	0					
Public	Waveforms	#	#	#	0	Hardware	#	1	取得CH1 Scope數據	1000					
Public	Get Measurement	#	#	#	0	Hardware	#	1	顯示CH1 Scope數據						
Public	Save Screen	#	#	#	0	Hardware	#	1	擷取CH1眼圖	QSFP10-SR4 TEST	Single Mask				
None	#	#	#	#	0	#	#	#	-------CH2-------						
Public	Switch Channel	#	#	#	0	Hardware	#	2	設定CH2						
Public	Get Temp	#	#	0	0	Firmware Test	#	#	取得Temp數據	1					
Public	Get Voltage	#	#	0	0	Firmware Test	#	#	取得Voltage數據	1					
Public	Waveforms	#	#	#	0	Hardware	#	2	取得CH2 Scope數據	1000					
Public	Get Measurement	#	#	#	0	Hardware	#	2	顯示CH2 Scope數據						
Public	Save Screen	#	#	#	0	Hardware	#	2	擷取CH2眼圖	QSFP10-SR4 TEST	Single Mask				
None	#	#	#	#	0	#	#	#	-------CH3-------						
Public	Switch Channel	#	#	#	0	Hardware	#	3	設定CH3						
Public	Get Temp	#	#	0	0	Firmware Test	#	#	取得Temp數據	2					
Public	Get Voltage	#	#	0	0	Firmware Test	#	#	取得Voltage數據	2					
Public	Waveforms	#	#	#	0	Hardware	#	3	取得CH3 Scope數據	1000					
Public	Get Measurement	#	#	#	0	Hardware	#	3	顯示CH3 Scope數據						
Public	Save Screen	#	#	#	0	Hardware	#	3	擷取CH3眼圖	QSFP10-SR4 TEST	Single Mask				
None	#	#	#	#	0	#	#	#	-------CH4-------						
Public	Switch Channel	#	#	#	0	Hardware	#	4	設定CH4						
Public	Get Temp	#	#	0	0	Firmware Test	#	#	取得Temp數據	3					
Public	Get Voltage	#	#	0	0	Firmware Test	#	#	取得Voltage數據	3					
Public	Waveforms	#	#	#	0	Hardware	#	4	取得CH4 Scope數據	1000					
Public	Get Measurement	#	#	#	0	Hardware	#	4	顯示CH4 Scope數據						
Public	Save Screen	#	#	#	0	Hardware	#	4	擷取CH4眼圖	QSFP10-SR4 TEST	Single Mask				
Public	Report CSV	#	#	#	0	Report	#	#	存取Scope數據	QSFP10-SR4 TEST	Sync				
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Close USB-I2C	#	#	#	0	Module	#	#	Close USB-I2C						
End	#	#	#	#	0	#	#	#	測試結束						
