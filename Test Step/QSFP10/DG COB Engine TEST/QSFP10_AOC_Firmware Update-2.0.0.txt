Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V2.0.0
Public	Multi FW Update	#	0	#	0	Test Item	#	All	Initialize Table						
None	Start	#	#	#	0	#	#	#	-------Test Step-------		
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	DG Server					
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	COB QSFP28				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Firmware Update	#	#	#	0	COB	#	#	Firmware Update	QSFP10					
Public	Voltage3.3	10	3.6	3	0	COB	#	#	Check Voltage3.3						
Public	Voltage1.8	10	1.9	1.7	0	COB	#	#	Check Voltage1.8						
Public	Set Lot Number	#	#	#	0	COB	#	#	Set Lot Number						
Public	Get SMT Lot Number	#	#	#	0	COB	#	#	Get SMT Lot Number						
Public	Get Lot Number	#	#	#	0	COB	#	#	Get Lot Number
#Public	Input Parameter	#	#	#	0	MES	#	#	Check MES Parameter	FALSE						
#Public	Move In	#	#	#	0	MES	#	#	MES Move In	0	COB	"溼恀MES炵苀囮啖 : 蜆馱等<%s> , MES隙換渣昫捅洘峈 : %s"					
Public	Download WID File	#	#	#	0	Cloud	#	#	Get WID Number	DG Server					
None	#	#	#	#	0	#	#	#	-------Tx Setting-------						
TRx Device	Set Burn-in	#	#	#	0	VCSEL Driver	#	All	Enable Burn-in Mode	Enable					
TRx Device	Set Burn-in Current	#	#	#	0	VCSEL Driver	#	All	Set Burn-in Current	Server					
None	#	#	#	#	0	#	#	#	-------Rx Setting-------						
TRx Device	Set Power Down	#	#	#	0	TIA Device	#	All	Rx Power Down	Enable	
Public	Get Tx Power From PD	3	99999	1000	0	COB	#	#	Get Tx Power From PD	QSFP10	100
TRx Device	Set Power Down	#	#	#	100	VCSEL Driver	#	All	Tx Power Up	Disable						
Public	Get Current	#	0	0	#	Hardware	#	1	Get Current	1
#Public	Move Out	#	90000	4000	0	MES	#	#	MES Move Out			COB	1		"杅擂奻換MES炵苀囮啖,MES隙換渣昫捅洘峈 : %s"	"杅擂奻換MES炵苀祑都,褫夔埻秪峈1.蜆萇齟厙繚眒剿盄, 2.厙繚祥恛隅, 3.MES炵苀蟀盄袨怓祑都"					
Public	Firmware Update	#	#	#	0	Report	#	#	Report CSV File	1_QSFP10_AOC-Firmware Update					
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	QSFP10_AOC Product Line\1_QSFP10_AOC-Firmware Update
Public	Arrange By LotCode	#	#	#	#	Report	#	#	Copy Report to LotCode Folder					
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result	COB					
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C		
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	DG Server					
End	#	#	#	#	0	#	#	#	Test Done						
