Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	AOC Calibration&EEPROM	#	0	#	0	Test Item	#	4	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	100	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	100	Hardware	#	#	Configure PSU	Power Supply	SFP-DD 100G DSP				
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
Public	Connect	#	#	#	0	Cloud	#	#	Connect to Server	Dropbox					
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Search Product	#	#	#	0	AOC	#	#	Search Product Type	OQC		Disable Reset Table			LUX BERT
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	6	TW
Public	Set Customer Password	#	#	#	0	AOC	#	#	Set Customer Password						
Public	Get Product Temp Define	#	#	#	0	AOC	#	#	Get Product Temp Define						
Public	Verify PN Temp Define	#	#	#	0	AOC	#	#	Verify Temp Define						
Public	EEPROM Update	#	#	#	100	AOC	#	#	EEPROM Update				#Ethernet CMIS 4.0		
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Set Power State	#	#	#	6000	Hardware	#	#	Power ON	ON					
Public	EEPROM Check	#	#	#	100	AOC	#	#	EEPROM Check				#Ethernet CMIS 4.0		
Public	Version	#	#	#	100	AOC	#	All	Version	Custom	SFP-DD 100G DSP_*_AOC_*_V05*_EFM8LB1.hex	Old Version			
None	None	#	#	#	0	#	#	#	-------Calibration Step-------						
Public	Tx Bias Calibration	5	8	6	#	AOC	100	All	TX Bias Calibration	7	EEPROM				
Public	TxP Calibration	5	1	-1	#	AOC	100	All	TX Calibration	0	EEPROM				
Public	RxP Calibration	5	0	-2	#	AOC	100	All	RX Calibration	-1	EEPROM
Public	Set Temp Slope	#	#	#	#	Product	100	All	Set Temp Slope	125	7
Public	Set Temp Offset	#	#	#	#	Product	100	All	Set Temp Offset	-5					
Public	Threshold Check	#	#	#	#	AOC	#	All	Threshold Check	1					
Public	Serial Number Check	#	#	#	#	Product	#	#	Check Serial Number
Public	EEPROM Dump	#	#	#	#	Report	#	#	Dump EEPROM Data							
Public	Upload Product EEPROM	#	#	#	0	Cloud	#	#	Upload EEPROM Data
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	7	TW						
Public	Report CSV	#	#	#	0	Report	#	#	Report	SFP-DD 100G DSP Product Line\3_SR2 Test\EEPROM Update\SFP-DD DSP 16nm AOC_EEPROM Update					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SFP-DD 100G DSP Product Line\3_SR2 Test\EEPROM Update\SFP-DD DSP 16nm AOC_EEPROM Update				
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\3_SR2 Test\EEPROM Update\SFP-DD DSP 16nm AOC_EEPROM Update		Disable User Folder			
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Error Code	#	#	#	0	AOC	#	All	Get Error Code						
Public	Close USB-I2C	#	#	#	0	AOC	#	#	Close USB-I2C						
Public	Disconnect	#	#	#	0	Cloud	#	#	Disconnect Server	Dropbox					
Public	Set Power State	#	#	#	100	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
