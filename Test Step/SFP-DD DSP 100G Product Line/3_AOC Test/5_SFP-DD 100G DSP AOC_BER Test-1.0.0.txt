Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0				
Public	PAM4 DSP BER Test	#	0	#	0	Test Item	#	2	Initialize Table										
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info										
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF									
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP56 200G DSP BERT 59281								
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON				
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT	8								
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 FEC Test	
Public	Wait Current By Work Order	#	2	0.5	1000	Hardware	#	#	Wait DSP Current	5	100	10	Start Current						
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	SFP-DD	SFP-DD								Heat Flow
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage										
None	Start	#	#	#	0	#	#	#	-------Test Step-------										
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	4	TW		
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber					
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line										
Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Get DSP Info										
Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA										
Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	"3.2,3.4"	"3.2,3.4"	"1.7,1.9"
Public	Get Driver Data	#	#	#	100	VCSEL Driver	#	#	Get Vcsel Driver Data		Column	2							
Public	Get TIA Data	#	#	#	100	TIA Device	#	#	Get TIA Data		Column	2	5						
Public	Default Slope	#	#	#	100	Product	#	#	Default Rx Power Slope	Rx Power									
Public	Wait Temp Stable	#	50	30	100	PAM4 DSP	#	#	Wait Temp Stable	10	2	2	600						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable									
Public	Get DDMI	#	50	30	100	Product	#	#	Get MCU Temp	Temperature		MCU Temp							
Public	Get DDMI	3	28000	4000	100	Product	#	#	Get RSSI	RSSI	9		#Column					
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Disable MCU DDMI	Disable									
Public	Relock	#	#	#	4000	Hardware	#	#	BERT Relock										
Public	Get PAM4 FEC	#	4	0	#	Hardware	#	2	Get FEC Data	120	5.00E-08	10	Multi Product
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	6	TW						
None	Close	#	#	#	0	#	#	#	-------Close Step-------										
Public	Get Current	#	2	1.4	#	Hardware	#	1	Get Current
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record									
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage									
Public	Report CSV	#	#	#	0	Report	#	#	Report	SFP-DD 100G DSP Product Line\3_AOC Test\5_SFP-DD 100G DSP AOC_BER Test									
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SFP-DD 100G DSP Product Line\3_AOC Test\5_SFP-DD 100G DSP AOC_BER Test	TwoProduct		Result						
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\3_AOC Test	Result								
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C										
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF									
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect										
End	#	#	#	#	0	#	#	#	Test Done										
