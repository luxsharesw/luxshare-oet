Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Tx Eye Tune	#	0	#	0	Test Item	#	2	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	SFP-DD 100G DSP				
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	SFP-DD DSP 100G Tx Eye Tune				
Public	Connect	3	#	#	0	Hardware	#	#	Connect Scope	Scope					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Scope	Scope	QSFP-DD PAM4 DSP				
Public	Connect	3	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM				
Public	Wait Current	#	1	0.6	1000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current		
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	SFP-DD					
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable					
Public	Get ID Info	#	#	#	0	Product	#	#	Get Lot Number						
None	Start	#	#	#	0	#	#	#	----------Test Step----------						
Public	Get DSP Status	1	#	#	0	PAM4 DSP	#	#	Get DSP Info						
Public	Get Tx Info	1	#	#	0	PAM4 DSP	#	#	Get Line Side Data	Line-Side					
Public	Get Chip Status	#	#	#	0	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	0	PAM4 DSP	#	#	Get ADC	#	"3.2,3.4"	0.62	#		
Public	Get Driver Data	#	#	#	0	VCSEL Driver	#	#	Get Vcsel Driver Data						
Public	Wait Temp Stable	#	85	40	0	PAM4 DSP	#	#	Wait Temp Stable	20	1.5	1.5	600		
Public	Get DDMI	#	80	40	0	Product	#	#	Get MCU Temp	Temperature		MCU Temp			
None	#	#	#	#	0	#	#	#	-------------CH1-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	0	PAM4 DSP	#	1	Get Voltage	1					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	1	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	35	0	PAM4 DSP	#	1	Get DSP Temp	240					
Public	Switch Channel	#	#	#	1000	Hardware	#	1	Switch to CH1						
Public	Patterns	3	#	#	100	Hardware	#	#	CH1 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	1	CH1 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH1 - Verify Data	100G	PAM4 DSP AOC				
Public	Calibration TxP By Item	5	#	#	0	Product	#	1	Calibration Tx Power	Average Power	0.5				
Public	Save Screen	3	#	#	0	Hardware	#	1	CH1 - Save Eye Diagram	SFP-DD 100G DSP Product Line\5_SFP-DD 100G Save Eye Diagram					
None	#	#	#	#	0	#	#	#	-------------CH2-------------						
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	0	PAM4 DSP	#	2	Get Voltage	1					
Public	Get Temp	#	#	#	0	PAM4 DSP	#	2	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	35	0	PAM4 DSP	#	1	Get DSP Temp	240					
Public	Switch Channel	#	#	#	1000	Hardware	#	2	Switch to CH2						
Public	Patterns	3	#	#	100	Hardware	#	#	CH2 - Measurement Data						
Public	Get PAM4 Data	#	#	#	0	Hardware	#	2	CH2 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH2 - Verify Data	100G	PAM4 DSP AOC				
Public	Calibration TxP By Item	5	#	#	0	Product	#	2	Calibration Tx Power	Average Power	0.5				
Public	Save Screen	3	#	#	0	Hardware	#	2	CH2 - Save Eye Diagram	SFP-DD 100G DSP Product Line\5_SFP-DD 100G Save Eye Diagram					
Public	Calibration Tx Bias	5	8	6	0	Product	#	#	Calibration Tx Bias	7					
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	1	0.7	#	Hardware	#	1	Get Current						
Public	Report CSV	#	#	#	0	Report	#	#	Report	SFP-DD 100G DSP Product Line\5_SFP-DD 100G Save Eye Diagram					
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\5_SFP-DD 100G Save Eye Diagram					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SFP-DD 100G DSP Product Line\5_SFP-DD 100G Save Eye Diagram	Lot Number				
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\5_SFP-DD 100G Save Eye Diagram		Disable User Folder			
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
