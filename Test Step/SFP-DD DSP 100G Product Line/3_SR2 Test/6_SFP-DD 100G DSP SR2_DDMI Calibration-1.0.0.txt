Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	Engine DDMI Calibration	#	0	#	0	Test Item	#	2	初始化表格	PAM4					
None	Start	#	#	#	0	#	#	#	-------Test Step-------
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Power Supply	Power Supply	QSFP-DD 400G DSP				
Public	Set Power State	#	#	#	2000	Hardware	#	#	Power ON	ON					
Public	Wait Current By Work Order	#	1	0.5	1000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current	3	
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	SFP-DD
Public	Get ID Info	#	#	#	0	Product	#	#	Get Lot Number
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	5	TW					
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage						
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber					
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM				
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Meter	Power Meter					
Public	Configure	#	#	#	100	Hardware	#	#	Configure Power Meter	Power Meter	QSFP-DD 200G				
None	#	#	#	#	0	#	#	#	-------Test Step-------						
Public	Wait Temp Stable	#	80	30	0	PAM4 DSP	#	#	Wait Temp Stable	1	1.5	1.5	600		
Public	Set Tx Disable	#	#	#	100	Module	#	1	Tx 1 Disable	Disable	ByLane					
Public	Get Power	#	#	#	0	Hardware	5	1	Get Power	Tx Power	DBM	2			
Public	Set Tx Disable	#	#	#	100	Module	#	2	Tx 2 Disable	Disable	ByLane					
Public	Get Power	#	#	#	0	Hardware	5	2	Get Power	Tx Power	DBM	2		
Public	Set Tx Disable	#	#	#	100	Module	#	All	Tx Enable	Disable	
Public	Disable Check Tx Los	#	#	#	0	Product	#	#	Disable Check Tx Los								
None	#	#	#	#	0	#	#	#	-------Calibration-------				
Public	Default Slope	#	#	#	100	Product	#	#	Default Tx Bias	Tx Bias					
Public	Default Slope	#	#	#	100	Product	#	#	Default Tx Power Slope	Tx Power					
Public	Default Slope	#	#	#	100	Product	#	#	Default Rx Power Slope	Rx Power							
Public	Get Voltage	#	#	#	0	Module	#	1	Get Voltage	#3.25					
Public	Get Temperature	#	#	#	100	Module	#	1	Get Temp						
Public	Calibration Tx Bias	5	7.2	6.5	100	Product	#	#	Calibration Tx Bias	6.8					
Public	Tx Power Calibration By Row	#	#	#	500	Module	#	1	Tx Power Calibration	-1.1,-1.2	0.5				
Public	Set MCU DDMI	#	#	#	0	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Rx Power Calibration By PM	#	#	#	100	Hardware	5	#	Rx Power Calibration	0	-2.14,-2.13	1000
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage				
Public	Get Current	#	1.3	0.6	#	Hardware	#	1	Get Current						
Public	Get Computer Name	#	#	#	#	COB	#	#	Get Computer Name	
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	6	TW					
Public	Report CSV	#	#	#	500	Report	#	#	存取數據	SFP-DD 100G DSP Product Line\3_SR2 Test\6_SFP-DD 100G DSP SR2_DDMI Calibration					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SFP-DD 100G DSP Product Line\3_SR2 Test\6_SFP-DD 100G DSP SR2_DDMI Calibration	Lot Number		Result		
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\3_SR2 Test	Result			
None	Close	#	#	#	0	#	#	#	-------Close Step-------
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record						
Public	Close USB-I2C	#	#	#	100	COB	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	測試結束						
