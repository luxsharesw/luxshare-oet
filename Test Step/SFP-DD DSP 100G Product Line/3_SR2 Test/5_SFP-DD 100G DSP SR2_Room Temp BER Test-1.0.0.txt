Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Room Temp BER Test	#	0	#	0	Test Item	#	2	Initialize Table						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------
Public	Input Parameter	#	#	#	0	Work Order	#	#	Input Work Order Info						
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 FEC Test				
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply
Public	Set Power State	#	#	#	500	Hardware	#	#	Power OFF	OFF					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	SFP-DD 100G DSP				
Public	Set Power State	#	#	#	1000	Hardware	#	#	Power ON	ON
Public	Connect	2	#	#	100	Hardware	#	#	Connect Thermostream	Thermostream					
Public	Configure	2	#	#	100	Hardware	#	#	Configure Thermostream	Thermostream	DSP 59281 Product Line				
Public	Set Target Temp	2	#	#	0	Hardware	#	#	Set Target Temp	25						
Public	Wait Current By Work Order	#	1	0.5	1000	Hardware	#	#	Wait DSP Current	5	100	10	Start Current		
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	SFP-DD
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number
Public	Check Station	#	#	#	100	AOC	#	#	Check Station State	4	TW					
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage						
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Check Lotnumber	#	#	#	0	Work Order	#	#	Check Lotnumber					
Public	Check Product Line	#	#	#	0	Work Order	#	#	Check Prodcut Line						
Public	Get DSP Status	1	#	#	100	PAM4 DSP	#	#	Get DSP Info						
Public	Get Chip Status	#	#	#	100	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	100	PAM4 DSP	#	#	Get ADC	"3.2,3.4"	"3.2,3.4"	"1.7,1.9"
Public	Get Driver Data	#	#	#	100	VCSEL Driver	#	#	Get Vcsel Driver Data						
Public	Get TIA Data	#	#	#	100	TIA Device	#	#	Get TIA Data	Single					
Public	Wait Temp Stable	#	45	20	100	PAM4 DSP	#	#	Wait Temp Stable	10	2	2	600		
Public	Get DDMI	#	45	20	100	Product	#	#	Get MCU Temp	Temperature		DDMI Temp			
Public	Default Slope	#	#	#	100	Product	#	#	Default Rx Power Slope	Rx Power					
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get DDMI	3	15000	100	100	Product	#	#	Get RSSI	RSSI	0		Column		
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Disable MCU DDMI	Disable					
Public	Relock	#	#	#	2000	Hardware	#	#	BERT Relock						
Public	Get PAM4 FEC	#	4	0	#	Hardware	#	2	Get FEC Data	120	5.00E-08	2	Column
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage
Public	Set Station	#	#	#	100	AOC	#	#	Set Test State	5	TW			
None	Close	#	#	#	0	#	#	#	-------Close Step-------
Public	Edit Record	#	#	#	0	Work Order	#	#	Edit Workorder Record						
Public	Get Current	#	1	0.7	#	Hardware	#	1	Get Current						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	1	Get Final MCU Temp	MCU Final Temp					
Public	Get DSP Temp	#	45	20	0	PAM4 DSP	#	1	Get DSP Final Temp	1	DSP Final Temp				
Public	Report CSV	#	#	#	0	Report	#	#	Report	SFP-DD 100G DSP Product Line\3_SR2 Test\5_SFP-DD 100G DSP SR2_Room Temp BER Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SFP-DD 100G DSP Product Line\3_SR2 Test\5_SFP-DD 100G DSP SR2_Room Temp BER Test	Lot Number		Result		
Public	Copy Report By Workorder	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\3_SR2 Test	Result			
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
