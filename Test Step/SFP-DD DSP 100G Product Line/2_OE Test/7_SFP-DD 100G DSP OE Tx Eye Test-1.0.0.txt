Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP Tx Eye Tune	#	0	#	0	Test Item	#	2	Initialize Table	Tx Tune					
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	SFP-DD 100G DSP					
Public	Set Power State	#	#	#	500	Hardware	#	#	Power ON	ON					
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	SFP-DD DSP 100G Tx Eye Tune				
Public	Connect	3	#	#	0	Hardware	#	#	Connect Optical Switch	Optical Switch	MM				
Public	Connect	3	#	#	0	Hardware	#	#	Connect RF Switch	RF Switch					
Public	Select A Channel	#	#	#	0	Hardware	#	#	Select Clock Tx 1-4						
Public	Connect	3	#	#	100	Hardware	#	#	Connect Scope	Scope					
Public	Configure	#	#	#	0	Hardware	#	#	Configure Scope	Scope	QSFP-DD PAM4 DSP Tx Test				
Public	Wait Current	#	1	0.45	1000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	SFP-DD
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage					
Public	Get ID Info	#	#	#	100	Product	#	#	Get Lot Number						
None	Start	#	#	#	0	#	#	#	----------Test Step----------
Public	Get DSP Status	1	#	#	0	PAM4 DSP	#	#	Get DSP Info						
Public	Get Chip Status	#	#	#	0	PAM4 DSP	#	#	Check Vcsel Driver & TIA						
Public	Get ADC	1	#	#	0	PAM4 DSP	#	#	Get ADC	"3.2,3.4"	"3.2,3.4"	"1.7,1.9"		
Public	Get DDMI	#	80	30	0	Product	#	#	Get MCU Temp	Temperature		MCU Temp		
None	#	#	#	#	0	#	#	#	-------------CH1-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	1	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	1	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	100	PAM4 DSP	#	1	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	1	Switch to CH1						
Public	Patterns	3	#	#	100	Hardware	#	#	CH1 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	1	CH1 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH1 - Verify Data	200G	PAM4 DSP OE								
Public	Save Screen	3	#	#	0	Hardware	#	1	CH1 - Save Eye Diagram	SFP-DD 100G DSP Product Line\2_OE Test\7_SFP-DD 100G DSP OE Tx Eye Test
None	#	#	#	#	0	#	#	#	-------------CH2-------------						
Public	Set MCU DDMI	#	#	#	100	PAM4 DSP	#	#	Enable MCU DDMI	Enable					
Public	Get Voltage	#	#	#	100	PAM4 DSP	#	2	Get Voltage	1					
Public	Get Temp	#	#	#	100	PAM4 DSP	#	2	Get Temp	MCU Temp					
Public	Get DSP Temp	#	85	30	100	PAM4 DSP	#	2	Get DSP Temp	240					
Public	Switch Channel	3	#	#	1000	Hardware	#	2	Switch to CH2						
Public	Patterns	3	#	#	100	Hardware	#	#	CH2 - Measurement Data
Public	Get PAM4 Data	#	#	#	0	Hardware	#	2	CH2 - Get Data						
Public	Verify Measurement	#	#	#	0	Hardware	#	#	CH2 - Verify Data	200G	PAM4 DSP OE							
Public	Save Screen	3	#	#	0	Hardware	#	2	CH2 - Save Eye Diagram	SFP-DD 100G DSP Product Line\2_OE Test\7_SFP-DD 100G DSP OE Tx Eye Test
Public	Get Tx Info	1	#	#	0	PAM4 DSP	#	#	Get Line Side Data	Line-Side
Public	Get Driver Data	#	#	#	100	VCSEL Driver	#	#	Get Vcsel Driver Data	
Public	Check Voltage	#	3.3	3.0	100	Product	#	#	Check DDMI Voltage						
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	1.5	0.7	#	Hardware	#	1	Get Current						
Public	Report CSV	#	#	#	0	Report	#	#	Report	SFP-DD 100G DSP Product Line\2_OE Test\7_SFP-DD 100G DSP OE Tx Eye Test
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SFP-DD 100G DSP Product Line\2_OE Test\7_SFP-DD 100G DSP OE Tx Eye Test	Lot Number		Result		
Public	Copy Report By Lotnumber	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\2_OE Test			
Public	Rename Eye Diagram Folder	#	#	#	0	Report	#	#	Rename Eye Diagram Folder	SFP-DD 100G DSP Product Line\2_OE Test\7_SFP-DD 100G DSP OE Tx Eye Test	Result				
Public	Copy Eye Diagram	#	#	#	0	Report	#	#	Copy Eye Diagram	SFP-DD 100G DSP Product Line\2_OE Test\7_SFP-DD 100G DSP OE Tx Eye Test	Result	Lotnumber				
Public	Column Status	#	#	#	0	Function	#	#	Display Result						
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect								
End	#	#	#	#	0	#	#	#	Test Done						
