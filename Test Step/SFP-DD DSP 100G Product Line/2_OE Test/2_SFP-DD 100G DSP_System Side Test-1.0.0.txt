Folder	Class	Retest	Max	Min	Delay	Library	Sample	Channel	Note	Parameter1	Parameter2	Parameter3	Parameter4	Parameter5	V1.0.0
Public	PAM4 DSP System Side FEC Test	#	0	#	0	Test Item	#	2	Initialize Table	Updtae Fw						
None	Init	#	#	#	0	#	#	#	-------Initialize Step-------						
Public	Connect	#	#	#	0	Hardware	#	#	Connect Power Supply	Power Supply					
Public	Configure	#	#	#	0	Hardware	#	#	Configure PSU	Power Supply	QSFP56 200G DSP BERT 59281				
Public	Set Power State	#	#	#	1500	Hardware	#	#	Power ON	ON					
Public	Connect	#	#	#	0	Hardware	#	#	Connect BERT	BERT					
Public	Configure	3	#	#	0	Hardware	#	#	Configure BERT	BERT	PAM4 FEC Test				
Public	Wait Current	#	1	0.2	1000	Hardware	#	#	Wait DSP Current	5	100	10	Idle Current		
Public	Search Product	#	#	#	100	Product	#	#	Search Product Type	SFP-DD					
Public	Set Data Path Power	#	#	#	0	PAM4 DSP	#	1	Power Up	Enable					
None	Start	#	#	#	0	#	#	#	-------Test Step-------						
Public	Get IQC Lotnumber	#	#	#	100	Product	#	#	Get IQC Lot Number							
Public	Get DSP Status	1	#	#	0	PAM4 DSP	#	#	Get DSP Info						
Public	Get ADC	1	#	#	0	PAM4 DSP	#	#	Get ADC	"3.2,3.4"	"3.2,3.4"	"1.7,1.9"
Public	Set System Side Loopback	#	#	#	100	PAM4 DSP	#	#	System Side Remote Loopback	Remote	Enable				
Public	Wait Temp Stable	#	85	50	100	PAM4 DSP	#	#	Wait Temp Stable	1	1.5	1.5	600		
Public	Relock	#	#	#	5000	Hardware	#	#	BERT Relock
Public	Get System Side Input	#	#	#	0	PAM4 DSP	#	#	Get System Side Input	2	5.00E-08	1	Column	Sync						
Public	Get PAM4 FEC	#	4	0	#	Hardware	#	2	Get FEC Data	50	5.00E-08	2	Column
Public	Wait Sync	#	#	#	0	Function	#	#	Wait Get System Side Input Finish	FEC Test
Public	Get Current	#	1	0.2	0	Hardware	#	1	Get Current		Test				
None	Start	#	#	#	100	#	#	#	-------Update Burn In FW-------						
Public	Firmware Update By Lot Number	#	#	#	100	Product	#	#	Update Burn In Firmware	BurnIn	120000	Burn In Firmware	Result		
Public	Checksum	#	#	#	100	AOC	#	#	Check Checksum										
None	Close	#	#	#	0	#	#	#	-------Close Step-------						
Public	Get Current	#	1	0.2	0	Hardware	#	1	Get Current						
Public	Report CSV	#	#	#	0	Report	#	#	Report	SFP-DD 100G DSP Product Line\2_OE Test\2_SFD-DD 100G DSP_System Side Test					
Public	Rename Current CSV	#	#	#	#	Report	#	1	Rename CSV File	SFP-DD 100G DSP Product Line\2_OE Test\2_SFD-DD 100G DSP_System Side Test	Lot Number		Result		
Public	Copy Report	#	#	#	0	Report	#	#	Copy Report	SFP-DD 100G DSP Product Line\2_OE Test\2_SFD-DD 100G DSP_System Side Test		Disable User Folder			
Public	Close Product	#	#	#	100	Product	#	#	Close USB-I2C						
Public	Set Power State	#	#	#	0	Hardware	#	#	Power OFF	OFF					
Public	Disconnect	#	#	#	0	Hardware	#	#	Disconnect						
End	#	#	#	#	0	#	#	#	Test Done						
