Low Power Mode
Slave Address : A0 , Reg. Address : 03 , RW : 1 , Length :   1 , Delay :   10 , Data : 03
1. Bring the DUT up to Ready mode
Slave Address : A0 , Reg. Address : 1A , RW : 0 , Length :   1 , Delay :   4000 , Data : 00
Slave Address : A0 , Reg. Address : 03 , RW : 1 , Length :   1 , Delay :   10 , Data : 06
Slave Address : A0 , Reg. Address : 08 , RW : 1 , Length :   1 , Delay :   10 , Data : 01
Slave Address : A0 , Reg. Address : 08 , RW : 1 , Length :   1 , Delay :   10 , Data : 00
"2. Check page 1 byte 162 to ensure pre-cursor, post-cursor and amplitude controls are implemented,"
Slave Address : A0 , Reg. Address : 7F , RW : 0 , Length :   1 , Delay :   100 , Data : 01
Slave Address : A0 , Reg. Address : A2 , RW : 1 , Length :   1 , Delay :   10 , Data : 1C
"3. Read page 1 byte 153 and 154 to obtain the maximum settings,"
Slave Address : A0 , Reg. Address : 99 , RW : 1 , Length :   1 , Delay :   10 , Data : F0
Slave Address : A0 , Reg. Address : 9A , RW : 1 , Length :   1 , Delay :   10 , Data : 77
"4 Inspect the ApSel codes for an ApSel that occupies all lanes,"
Slave Address : A0 , Reg. Address : 7F , RW : 0 , Length :   1 , Delay :   100 , Data : 11
Slave Address : A0 , Reg. Address : CE , RW : 1 , Length :   8 , Delay :   10 , Data : 10 10 10 10 10 10 10 10
"5.Stage the Apsel code with Explicit Control set to 1,"
Slave Address : A0 , Reg. Address : 7F , RW : 0 , Length :   1 , Delay :   100 , Data : 10
Slave Address : A0 , Reg. Address : 91 , RW : 0 , Length :   8 , Delay :   10 , Data : 10 10 10 10 10 10 10 10
"6.Set the pre-cursor and post-cursor values to 1 less than its maximum value,"
Slave Address : A0 , Reg. Address : A2 , RW : 0 , Length :   8 , Delay :   10 , Data : 01 01 01 01 01 01 01 01
"7.Set the amplitude to the maximum value,"
Slave Address : A0 , Reg. Address : AA , RW : 0 , Length :   4 , Delay :   10 , Data : 03 03 03 03
"8.using Apply_DataPathInit configure the data path,"
Slave Address : A0 , Reg. Address : 8F , RW : 0 , Length :   1 , Delay :   1500 , Data : FF
"9.activate the data path,"
Slave Address : A0 , Reg. Address : 7F , RW : 0 , Length :   1 , Delay :   100 , Data : 11
Slave Address : A0 , Reg. Address : 80 , RW : 1 , Length :   4 , Delay :   10 , Data : 44 44 44 44
"10.check to ensure the data path configuration is accepted,"
Slave Address : A0 , Reg. Address : CA , RW : 1 , Length :   4 , Delay :   10 , Data : 11 11 11 11
"11.inspect the pre-cursor, post-cursor and amplitude to ensure they contain the configured settings.
Slave Address : A0 , Reg. Address : DF , RW : 1 , Length :   12 , Delay :   10 , Data : 01 01 01 01 01 01 01 01 03 03 03 03
